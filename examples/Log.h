#ifndef LOG_H
#define LOG_H

#include <QtCore/QDebug>

#define Log(x) qDebug(x) << __FUNCTION__ << __LINE__ << ":"

#define _STR(x) #x
#define STR(x) _STR(x)

#endif // LOG_H
