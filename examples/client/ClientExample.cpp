#include "ClientExample.h"
#include "Log.h"

ClientExample::ClientExample()
{
	QObject::connect(&m_client, &P4Client::requestSent,
					 this, &ClientExample::on_requestSent);
	QObject::connect(&m_client, &P4Client::requestFinished,
					 this, &ClientExample::on_requestFinished);
	QObject::connect(&m_client, &P4Client::requestError,
					 this, &ClientExample::on_requestError);

	Log() << "set connection:" << STR(P4_HOST) << P4_PORT;
	m_client.setConnectionInfo(STR(P4_HOST), P4_PORT);
}

void ClientExample::run()
{
	QSharedPointer<P4RequestUsers> reqUsers(QSharedPointer<P4RequestUsers>::create());
	QObject::connect(reqUsers.data(), &P4RequestUsers::usersChanged,
					 this, &ClientExample::on_usersChanged);

	m_client.send(reqUsers, "root");
}

void ClientExample::on_requestSent(QSharedPointer<P4Request> req)
{
	Log() << "cmd:" << req->fullCommand();
}

void ClientExample::on_requestFinished(QSharedPointer<P4Request> req, int msDuration)
{
	Log() << "cmd:" << req->fullCommand() << "status:" << req->result().toString() << "time:" << msDuration << "ms";
}

void ClientExample::on_requestError(QSharedPointer<P4Request> req)
{
	Log() << "status:" << req->result().toString();
}

void ClientExample::on_usersChanged(const QList<P4User> users)
{
	Log() << "new users list count: " << users.count();

//	foreach (P4User user, users)
//	{
//		Log() << user.name();
//	}
}
