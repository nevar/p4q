#ifndef CLIENTEXAMPLE_H
#define CLIENTEXAMPLE_H

#include <QtCore/QObject>
#include <p4q/P4Client.h>
#include <p4q/request/P4RequestUsers.h>

class ClientExample : public QObject
{
	Q_OBJECT
public:
	ClientExample();

	void run();

private:
	P4Client m_client;

private slots:
	// for signals from P4Client
	void on_requestSent(QSharedPointer<P4Request> req);
	void on_requestFinished(QSharedPointer<P4Request> req, int msDuration);
	void on_requestError(QSharedPointer<P4Request> req);

	// for signals from P4RequestUsers
	void on_usersChanged(const QList<P4User> users);
};

#endif // CLIENTEXAMPLE_H
