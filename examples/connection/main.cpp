/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <p4q/P4Connection.h>
#include <p4q/request/P4RequestUsers.h>
#include "Log.h"

int main(int argc, char *argv[])
{
	P4Connection m_p4Connection;
	m_p4Connection.setDefaultUser("root");

	Log() << "connect:" << STR(P4_HOST) << P4_PORT;
	m_p4Connection.connectToHost(STR(P4_HOST), P4_PORT);

	QSharedPointer<P4RequestUsers> req(new P4RequestUsers());
	Log() << "send request:" << req.data();

	m_p4Connection.sendSync(req);

	Log() << "request finished:" << req->result().toString();
	Log() << "users:" << req->getUsers().count();

	return 0;
}
