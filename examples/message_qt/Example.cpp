#include "Example.h"
#include "Log.h"

Example::Example(QObject *parent)
	: QObject(parent)
{
	// this makes that transport in/out messages will be visible on debug output
	m_transport.showMessagesInLog(true, true);

	QObject::connect(&m_transport, &P4Transport::connectedToHost,
					 this, &Example::on_connectedToHost);
	QObject::connect(&m_transport, &P4Transport::messagesReadyRead,
					 this, &Example::on_messagesReadyRead);
	QObject::connect(&m_transport, &P4Transport::transmissionFinished,
					 this, &Example::on_transmissionFinished);
}

void Example::run()
{	
	Log() << "connecting to host";
	m_transport.connectToHostAsync(STR(P4_HOST), P4_PORT);
}

void Example::on_connectedToHost()
{
	Log() << "sending request";

	P4Message msg;

	// send protocol init message
	msg.appendItem("cmpfile", "");
	msg.appendItem("client", "80");
	msg.appendItem("tag", "");
	msg.appendItem("api", "99999");
	msg.appendItem("enableStreams", "");
	msg.appendItem("expandAndmaps", "");
	msg.appendItem("host", "AMDC.local");
	msg.appendItem("port", "106.120.130.174@1888");
	msg.appendItem("sndbuf", "523552");
	msg.appendItem("rcvbuf", "525600");
	msg.appendItem("func", "protocol");
	m_transport.send(msg);
	Log() << "sent: " << msg.value("func");

	// send "p4 users" message
	msg.clearItems();
	msg.appendItem("client", "");
	msg.appendItem("cwd", "");
	msg.appendItem("os", "UNIX");
	msg.appendItem("user", "root");
	msg.appendItem("charset", "1");
	msg.appendItem("func", "user-users");
	m_transport.send(msg);
	Log() << "sent: " << msg.value("func");
}

void Example::on_messagesReadyRead()
{
	// process all input messages until "release" message indicating end of request processing
	QList<P4Message*> msgs = m_transport.getAllMessages();
	m_messagesReceived += msgs.count();

	foreach (P4Message* m, msgs)
	{
		if (m->containsKey("User"))
			++m_usersReceived;
		else
			Log() << m->value("func");

		delete m;
	}
	msgs.clear();
}

void Example::on_transmissionFinished()
{
	m_transport.dropWaitingMessages();
	Log() << "messages received " << m_messagesReceived;
	Log() << "users received    " << m_usersReceived;

	exit(0);
}
