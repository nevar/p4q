#ifndef EXAMPLE_H
#define EXAMPLE_H

#include <QtCore/QObject>
#include <p4q/data/P4Message.h>
#include <p4q/P4Transport.h>

class Example : public QObject
{
	Q_OBJECT
public:
	explicit Example(QObject *parent = nullptr);
	void run();

signals:

public slots:
	void on_connectedToHost();
	void on_messagesReadyRead();
	void on_transmissionFinished();

private:
	P4Transport m_transport;
	int m_usersReceived{0};
	int m_messagesReceived{0};
};

#endif // EXAMPLE_H