/*
 * Copyright (c) 2018 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <p4q/data/P4Message.h>
#include <p4q/P4Transport.h>
#include "Log.h"

int main(int argc, char *argv[])
{
	P4Transport transport;
	// this makes that transport in/out messages will be visible on debug output
	transport.showMessagesInLog(true, true);

	P4Result res = transport.connectToHostSync(STR(P4_HOST), P4_PORT);
	Log() << res.toString();

	P4Message msg;

	// send protocol init message
	msg.appendItem("cmpfile", "");
	msg.appendItem("client", "80");
	msg.appendItem("tag", "");
	msg.appendItem("api", "99999");
	msg.appendItem("enableStreams", "");
	msg.appendItem("expandAndmaps", "");
	msg.appendItem("host", "AMDC.local");
	msg.appendItem("port", "106.120.130.174@1888");
	msg.appendItem("sndbuf", "523552");
	msg.appendItem("rcvbuf", "525600");
	msg.appendItem("func", "protocol");
	transport.send(msg);
	Log() << "sent: " << msg.value("func");

	// send "p4 users" message
	msg.clearItems();
	msg.appendItem("client", "");
	msg.appendItem("cwd", "");
	msg.appendItem("os", "UNIX");
	msg.appendItem("user", "root");
	msg.appendItem("charset", "1");
	msg.appendItem("func", "user-users");
	transport.send(msg);
	Log() << "sent: " << msg.value("func");

	int usersReceived = 0;
	int messagesReceived = 0;
	bool transmissionCompleted = false;

	// process all input messages until "release" message indicating end of request processing
	while (transport.waitForMessages())
	{
		QList<P4Message*> msgs = transport.getAllMessages();
		messagesReceived += msgs.count();

		foreach (P4Message* m, msgs)
		{
			if (m->containsKey("User"))
				++usersReceived;

			if (m->value("func") == "release")
			{
				transmissionCompleted = true;
			}
			delete m;
		}
		msgs.clear();

		if (transmissionCompleted)
			break;
	}

	Log() << "messages received " << messagesReceived;
	Log() << "users received    " << usersReceived;

	if (!transmissionCompleted)
	{
		Log() << "error transmission not finished, possible lost messages";
	}

	return 0;
}
