#include "Log.h"
#include "p4q/data/P4Message.h"
#include <QtCore/QDebug>
#include <QtCore/QtGlobal>
#include <QtCore/QFileInfo>
#include <string.h>

QMutex Log::m_mutex(QMutex::Recursive);
QStringList Log::m_allowedLogTypes;
FILE* Log::m_logOutput = stdout;

static void __attribute__ ((constructor)) initLogLevels()
{
	Log::m_allowedLogTypes = QString::fromLatin1(qgetenv("P4DEBUG")).split(',');
}

Log::Log()
	: QTextStream(&m_data, QIODevice::ReadWrite)
{
}

Log::~Log()
{
	flush();
	m_mutex.lock();
	QTextStream out(m_logOutput, QIODevice::WriteOnly);
	out << m_data;
	m_mutex.unlock();
}

void Log::transport(QByteArray& data)
{
	if (!isPrintable(P4Config::LOG_TAG_TRANSPORT))
		return;

	QByteArray line;

	int count = 0;
	int i = 0;
	while (i < data.size())
	{
		line.append(data[i]);
		if (++count >= 32)
		{
#if QT_VERSION >= 0x050900
			*this << line.toHex(' ');
#else
			*this << line.toHex();
#endif
			for (int k = 0; k < line.size(); ++k)
			{
				if (!QChar(line[k]).isPrint())
					line[k] = '.';
			}
			*this << " " << line << "\n";
			line.clear();
			count = 0;
		}
		i++;
	}
}

void Log::message(const P4Message* msg)
{
	if (!isPrintable(P4Config::LOG_TAG_MESSAGE))
		return;

	int max = 0;
	foreach (P4Pair p, msg->itemList())
	{
		max = qMax(max, p.first.count());
	}

	foreach (P4Pair p, msg->itemList())
	{
		*this << p.first;
		int space = max - p.first.size();
		while(space--)
		{
			*this << " ";
		}
		*this << " | " << p.second << "\n";
	}
}

void Log::command(const P4Message* msg)
{
	if (!isPrintable(P4Config::LOG_TAG_COMMAND))
		return;

	QString command("$ p4 ");
	bool isCommand = false;

	foreach (P4Pair p, msg->itemList())
	{
		if (p.first == "func" && p.second.startsWith("user-"))
		{
			isCommand = true;
			command.append(p.second.remove("user-"));
		}
	}

	if (isCommand)
	{
		foreach (P4Pair p, msg->itemList())
		{
			if (p.first.isEmpty())
			{
				command.append(QString(" %1").arg(p.second));
			}
		}

		*this << command << "\n";
	}
}

QString Log::getPrefix(const char* file, int line, P4Config::LogTag tag)
{
	QFileInfo fileInfo(file);
	return QString("[%3][%1:%2]\t> ").arg(fileInfo.baseName()).arg(line).arg(logTypes()[tag]);
}

QStringList Log::logTypes()
{
	static QStringList list;

	if (list.isEmpty())
	{
		list << "INF" << "DBG" << "ERR" << "CMD" << "SIO" << "MSG" << "RAW" << "ALL";
	}

	return list;
}

bool Log::isPrintable(P4Config::LogTag tag)
{
	return m_allowedLogTypes.contains(logTypes()[tag]) ||
			m_allowedLogTypes.contains(logTypes()[P4Config::LOG_TAG_ALL]) ||
			tag == P4Config::LOG_TAG_ERROR;
}
