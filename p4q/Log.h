#ifndef LOG_H
#define LOG_H

#include <QtCore/QMutex>
#include <QtCore/QTextStream>
#include <p4q/P4Config.h>
#include <QtCore/QThread>

#define _ptr(x) QString::number((quintptr)(x), 16)

#define LOG_THREAD QString("[%1]").arg(_ptr(QThread::currentThreadId()))
#define LOG_PREFIX(x) Log::getPrefix(__FILE__, __LINE__, x)
#define LOG(x, level) do { \
	if (Log::isPrintable(level)) \
	{ \
		Log() << LOG_THREAD << LOG_PREFIX(level) << x << "\n"; \
	} } while(0)

#define ERR(x) LOG(x, P4Config::LOG_TAG_ERROR)
#define INF(x) LOG(x, P4Config::LOG_TAG_INFO)
#define DBG(x) LOG(x, P4Config::LOG_TAG_DEBUG)
#define SIO(x) LOG(x, P4Config::LOG_TAG_SHORT_INPUT_OUTPUT)

#define LOG_COMMAND(obj, x) obj.command(x)
#define LOG_TRANSPORT(obj, x) obj.transport(x)
#define LOG_MESSAGE(obj, x) obj.message(x)

class P4Message;

class Log : public QTextStream
{
public:
	Log();
	~Log();

	void transport(QByteArray& data);
	void message(const P4Message* msg);
	void command(const P4Message* msg);

	static QString getPrefix(const char* file, int line, P4Config::LogTag tag);
	static QStringList logTypes();
	static bool isPrintable(P4Config::LogTag type);

public:
	static QStringList m_allowedLogTypes;

private:
	QByteArray m_data;
	static QMutex m_mutex;
	static FILE* m_logOutput;
};

#endif // LOG_H
