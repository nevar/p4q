/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "p4q/P4Client.h"
#include "p4q/P4Client_p.h"
#include "p4q/request/P4Request_p.h"
#include "p4q/Log.h"
#include "p4q/request/P4Request.h"
#include <QtCore/QThread>
#include <QtCore/QMutexLocker>

#ifdef P4_REQ_DEBUG
#define PrintReqStatus d_ptr->printReqStatus()
#else
#define PrintReqStatus
#endif

P4Client::P4Client(QObject* parent)
	: QObject(parent)
	, d_ptr(new P4ClientPrivate(this))
{
}

void P4Client::send(QSharedPointer<P4Request> req, const QString& user, const QString& workspace)
{
	// TODO what if user will pass same request several times before it was finished?
	Q_D(P4Client);
	req->setUser(user);
	req->setWorkspace(workspace);
	QMutexLocker lock(&d->m_dataMutex);

	if (d->m_idleProtocol.isEmpty())
	{
		d->queueAdd(req);
		d->spawnNewProtocol();
		return;
	}

	P4Connection *protocol = *d->m_idleProtocol.begin();
	d->runRequest(protocol, req);
}

QString P4Client::host() const
{
	Q_D(const P4Client);
	QMutexLocker lock(&d->m_dataMutex);
	return d->m_host;
}

void P4Client::setConnectionInfo(const QString& aHost, qint16 aPort)
{
	Q_D(P4Client);
	QMutexLocker lock(&d->m_dataMutex);

	d->dropAllConnections();
	d->dropAllPendingRequests();

	d->m_host = aHost;
	d->m_port = aPort;
}

void P4Client::abort()
{
	Q_D(P4Client);
	QMutexLocker lock(&d->m_dataMutex);

	d->dropAllConnections();
	d->dropAllPendingRequests();
}

qint16 P4Client::port() const
{
	Q_D(const P4Client);
	QMutexLocker lock(&d->m_dataMutex);
	return d->m_port;
}

void P4Client::setDefaultUser(QString aUser)
{
	Q_D(P4Client);
	d->m_defaultUser = aUser;
}

QString P4Client::defaultUser() const
{
	Q_D(const P4Client);
	return d->m_defaultUser;
}
