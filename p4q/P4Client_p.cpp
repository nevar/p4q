/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "p4q/P4Client_p.h"
#include "p4q/P4Client.h"
#include "p4q/request/P4Request.h"
#include "p4q/request/P4Request_p.h"
#include "p4q/Log.h"

P4ClientPrivate::P4ClientPrivate(P4Client* q)
	: q_ptr(q)
	, m_dataMutex(QMutex::Recursive)
	, m_port(0)
{
}

void P4ClientPrivate::on_protocolConnected()
{
	Q_Q(P4Client);
	P4Connection* connection = dynamic_cast<P4Connection*>(QObject::sender());
	Q_ASSERT(connection);

	QMutexLocker lock(&m_dataMutex);
	m_initProtocol.remove(connection);
	m_idleProtocol.insert(connection);
	printReqStatus();	// init -> idle changed
	processNextRequest(connection);
}

void P4ClientPrivate::on_protocolDisconnected(P4Result res)
{
	Q_Q(P4Client);
//	emit q->requestError(res); FIXME send this for each request ?
	P4Connection* connection = dynamic_cast<P4Connection*>(QObject::sender());
	Q_ASSERT(connection);
	deleteProtocol(connection);
}

void P4ClientPrivate::on_protocolRequestFinished(QSharedPointer<P4Request> req)
{
	Q_Q(P4Client);
	P4Connection* connection = dynamic_cast<P4Connection*>(QObject::sender());
	Q_ASSERT(connection);

	// FIXME Add getting time of request
	emit q->requestFinished(req, m_reqTime[req.data()].elapsed());
	m_reqTime.remove(req.data());
	processNextRequest(connection);
}

void P4ClientPrivate::queueAdd(QSharedPointer<P4Request> req)
{
	m_reqWaiting.prepend(req);
	printReqStatus(); // queue changed (add)
}

QSharedPointer<P4Request> P4ClientPrivate::queueGet()
{
	QSharedPointer<P4Request> req(m_reqWaiting.takeFirst());
	printReqStatus(); // queue changed (remove)
	return req;
}

void P4ClientPrivate::processNextRequest(P4Connection* protocol)
{
	QMutexLocker lock(&m_dataMutex);

	if (!m_reqWaiting.isEmpty())
	{
		runRequest(protocol, queueGet());
		printReqStatus();	// queue changed
	}
	else
	{
		if (m_busyProtocol.remove(protocol))
		{
			m_idleProtocol.insert(protocol);
			printReqStatus();	// busy -> idle changed
		}
		else
		{
			if (!m_idleProtocol.contains(protocol))
			{
				deleteProtocol(protocol);
			}
		}
	}
}

void P4ClientPrivate::runRequest(P4Connection* protocol, QSharedPointer<P4Request> req)
{
	Q_Q(P4Client);

	// FIXME: check this logic it is very unclear
	if (!m_busyProtocol.contains(protocol))
	{
		if (!m_idleProtocol.remove(protocol))
		{
			queueAdd(req);
			deleteProtocol(protocol);
			spawnNewProtocol();
			return;
		}

		m_busyProtocol.insert(protocol);
		printReqStatus(); // idle -> busy changed
	}

	emit q->requestSent(req);
	m_reqTime[req.data()].start();
	QMetaObject::invokeMethod(protocol, "send", Qt::QueuedConnection, Q_ARG(QSharedPointer<P4Request>, req));
}

// FIXME: this function prints notihng, change its name to update or smth different
void P4ClientPrivate::printReqStatus() const
{
	Q_Q(const P4Client);

	emit q->stateChanged(m_reqWaiting.count(), m_idleProtocol.count(), m_busyProtocol.count());

#ifdef P4_REQ_DEBUG
	DBG("status"
		<< "waiting:" << m_reqWaiting.count()
		<< "connections:" << m_busyProtocol.count()
		<< "/" << m_idleProtocol.count()+m_busyProtocol.count());
#endif
}

void P4ClientPrivate::spawnNewProtocol()
{
	if (threadsStarted() < m_threadsMax)
	{
		QThread *th = new QThread(); // TODO: Check when th will be deleted
		P4Connection *protocol = new P4Connection();
		protocol->moveToThread(th);
		th->start();

		QObject::connect(protocol, &P4Connection::connected,
						 this, &P4ClientPrivate::on_protocolConnected);
		QObject::connect(protocol, &P4Connection::disconnected,
						 this, &P4ClientPrivate::on_protocolDisconnected);
		QObject::connect(protocol, &P4Connection::requestFinished,
						 this, &P4ClientPrivate::on_protocolRequestFinished);

		protocol->setDefaultUser(m_defaultUser);

		m_initProtocol.insert(protocol);
		printReqStatus(); // new protocol created

		QMetaObject::invokeMethod(protocol, "connectToHost",
								  Qt::QueuedConnection,
								  Q_ARG(QString, m_host),
								  Q_ARG(quint16, m_port));
	}
}

bool P4ClientPrivate::deleteProtocol(P4Connection* protocol)
{
	Q_Q(P4Client);
	bool changed = false;

	if (m_idleProtocol.remove(protocol))
		changed = true;

	if (m_busyProtocol.remove(protocol))
		changed = true;

	m_initProtocol.remove(protocol);

	protocol->deleteLater();

	if (changed)
		printReqStatus();

	return changed;
}

void P4ClientPrivate::dropAllConnections()
{
	foreach (P4Connection* protocol, m_idleProtocol)
	{
		deleteProtocol(protocol);
	}
}

// FIXME: It is drop pending or rather waiting requests?
void P4ClientPrivate::dropAllPendingRequests()
{
	Q_Q(P4Client);

	foreach (QSharedPointer<P4Request> req, m_reqWaiting)
	{
		req->d_ptr->appendResult("Request cancelled", P4RESULT_WARNING);
		emit q->requestError(req);
	}
	m_reqWaiting.clear();
	m_reqTime.clear();
}

int P4ClientPrivate::threadsStarted() const
{
	return m_idleProtocol.count() + m_busyProtocol.count() + m_initProtocol.count();
}

int P4ClientPrivate::threadsConnected() const
{
	return m_idleProtocol.count() + m_busyProtocol.count();
}
