/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef P4CLIENTPRIVATE_P_H
#define P4CLIENTPRIVATE_P_H

#include "p4q/P4Connection.h"
#include "p4q/data/P4Result.h"
#include <QtCore/QQueue>
#include <QtCore/QHash>
#include <QtCore/QSet>
#include <QtCore/QMap>
#include <QtCore/QObject>
#include <QtCore/QThread>
#include <QtCore/QTime>
#include <QtCore/QMutex>
#include <QtCore/QSharedPointer>

class P4Client;
class P4Request;

class P4ClientPrivate : public QObject
{
	Q_OBJECT
	Q_DECLARE_PUBLIC(P4Client)
	// FIXME firending is already assigned by macro
	friend class P4Client;

private:
	P4ClientPrivate(P4Client* q);

private slots:
	void on_protocolConnected();
	void on_protocolDisconnected(P4Result res);
	void on_protocolRequestFinished(QSharedPointer<P4Request> req);

private:
	void queueAdd(QSharedPointer<P4Request> req);
	QSharedPointer<P4Request> queueGet();
	void processNextRequest(P4Connection* protocol);
	void runRequest(P4Connection* protocol, QSharedPointer<P4Request> req);
	void printReqStatus() const;
	void spawnNewProtocol();
	bool deleteProtocol(P4Connection *protocol);
	void dropAllConnections();
	void dropAllPendingRequests();
	int threadsStarted() const;
	int threadsConnected() const;

private:
	P4Client *q_ptr;

	static const unsigned int m_threadsMax = 8;

	mutable QMutex m_dataMutex;
	QQueue<QSharedPointer<P4Request>> m_reqWaiting;
	QMap<const P4Request*, QTime> m_reqTime;
	QSet<P4Connection*> m_idleProtocol;
	QSet<P4Connection*> m_busyProtocol;
	QSet<P4Connection*> m_initProtocol;
	QString m_host;
	qint16 m_port;
	QString m_defaultUser;
};

#endif // P4CLIENTPRIVATE_P_H
