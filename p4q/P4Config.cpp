#include "p4q/P4Config.h"
#include "Log.h"

void P4Config::setLogTag(LogTag tag)
{

}

void P4Config::addLogTag(P4Config::LogTag tag)
{
	if (!Log::m_allowedLogTypes.contains(Log::logTypes()[tag]))
	{
		Log::m_allowedLogTypes.append(Log::logTypes()[tag]);
	}
}

void P4Config::removeLogTag(P4Config::LogTag tag)
{
	Log::m_allowedLogTypes.removeAll(Log::logTypes()[tag]);
}

void P4Config::removeAllLogTags()
{
	Log::m_allowedLogTypes.clear();
}
