/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "p4q/P4Connection_p.h"
#include "p4q/request/P4Request.h"
#include "p4q/request/P4Tag.h"
#include "p4q/P4Connection.h"
#include "p4q/request/P4Request_p.h"
#include "p4q/data/P4Error.h"
#include "p4q/Log.h"

P4Connection::P4Connection(QObject* parent)
	: QObject(parent)
	, d_ptr(new P4ConnectionPrivate(this))
{
}

P4Connection::~P4Connection()
{
}

void P4Connection::setDefaultUser(QString aUser)
{
	Q_D(P4Connection);
	d->m_defaultUser = aUser;
}

QString P4Connection::getDefaultUser() const
{
	Q_D(const P4Connection);
	return d->m_defaultUser;
}

P4Result P4Connection::connectToHost(QString host, quint16 port, int timeout)
{
	Q_D(P4Connection);

	if (!d->m_transport)
	{
		d->m_transport = new P4Transport(this);
		QObject::connect(d->m_transport, &P4Transport::messagesReadyRead,
						 d, &P4ConnectionPrivate::on_messageReady);
		QObject::connect(d->m_transport, &P4Transport::connectedToHost,
						 d, &P4ConnectionPrivate::on_transportConnected);
		QObject::connect(d->m_transport, &P4Transport::disconnectedFromHost,
						 d, &P4ConnectionPrivate::on_transportDisconnected);
	}

	return d->m_transport->connectToHostSync(host, port, timeout);
}

P4Result P4Connection::disconnectFromHost(int timeout)
{
	Q_D(P4Connection);

	return d->m_transport->disconnectFromHostSync(timeout);
}

bool P4Connection::isConnected() const
{
	Q_D(const P4Connection);

	if (!d->m_transport)
		return false;

	return d->m_transport->isConnected();
}

void P4Connection::send(QSharedPointer<P4Request> req)
{
	Q_D(P4Connection);
	Q_ASSERT(d->m_pendingRequest.isNull());
	Q_ASSERT(d->m_transport != nullptr);

	DBG(QString("Prepare to send request %1").arg(_ptr(req.data())));

	d->m_pendingRequest = req;
	d->m_isPendingAsync = true;

	if (d->m_pendingRequest->user().isNull())
	{
		DBG(QString("Setting user %2 for request %1").arg(_ptr(req.data())).arg(d->m_defaultUser));
		d->m_pendingRequest->setUser(d->m_defaultUser);
	}

	d->m_transport->dropWaitingMessages();

	// send protocol message if needed
	if (d->m_transport->messagesSentCount() == 0)
	{
		d->m_transport->send(d->newMsgProtocol());
	}

	// create & send request
	// we need friend here
	P4Message msg(req->d_ptr->newMsg(*this));
	req->d_ptr->started(); // TODO: started should be invoked here or by transport?
	d->m_transport->send(msg);
}

void P4Connection::sendSync(QSharedPointer<P4Request> req)
{
	Q_D(P4Connection);

	send(req);

	DBG(QString("Process reply from request %1").arg(_ptr(req.data())));
	while (!d->m_transport->canSend())
	{
		if (d->m_transport->waitForMessages())
			d->on_messageReady();
	}
}

QString P4Connection::host() const
{
	Q_D(const P4Connection);
	return d->m_transport->host();
}

qint16 P4Connection::port() const
{
	Q_D(const P4Connection);
	return d->m_transport->port();
}
