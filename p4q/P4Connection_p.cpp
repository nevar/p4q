#include "p4q/P4Connection_p.h"
#include "p4q/P4Connection.h"
#include "p4q/request/P4Tag.h"
#include "p4q/request/P4Request_p.h"
#include <QtCore/QCryptographicHash>
#include <QtCore/QFile>
#include <QtCore/QDir>
#include "p4q/Log.h"

P4ConnectionPrivate::P4ConnectionPrivate(P4Connection* q)
	: q_ptr(q)
{

}

P4Message P4ConnectionPrivate::newMsgProtocol() const
{
	QString connectionStr(QString("%1@%2")
						  .arg(m_transport->host())
						  .arg(m_transport->port()));
	P4Message msg;
	// FIXME fill proper values
	msg.appendItem(P4Tag2::k_cmpfile, "");
	msg.appendItem(P4Tag2::k_client, "80");
	msg.appendItem("tag", "");
	msg.appendItem(P4Tag2::k_api, "99999");
	msg.appendItem(P4Tag2::k_enablestreams, "");
	msg.appendItem(P4Tag2::k_expandandmaps, "");
	msg.appendItem(P4Tag2::k_host, "AMDC.local");
	msg.appendItem(P4Tag2::k_port, connectionStr);
	msg.appendItem(P4Tag2::k_sndbuf, "523552");
	msg.appendItem(P4Tag2::k_rcvbuf, "525600");
	msg.appendItem(P4Tag2::k_func, "protocol");
	return msg;
}

void P4ConnectionPrivate::messageProtocolHandler(P4Message* msg)
{
	Q_ASSERT(msg->value(P4Tag2::k_func) == P4Tag2::v_protocol);

	m_protocolProxy = msg->value(P4Tag2::k_proxy).toInt();
	m_protocolBroker = msg->value(P4Tag2::k_broker).toInt();
	m_protocolXFiles = msg->value(P4Tag2::k_xfiles).toInt();
	m_protocolServer = msg->value(P4Tag2::k_server).toInt();
	m_protocolServer2 = msg->value(P4Tag2::k_server2).toInt();
	m_protocolServerId = msg->value(P4Tag2::k_serverid);
	m_protocolRevVer = msg->value(P4Tag2::k_revver).toInt();
	m_protocolTZOffset = msg->value(P4Tag2::k_tzoffset).toInt();
	m_protocolSecurity = msg->value(P4Tag2::k_security).toInt();
	m_protocolSndBufSize = msg->value(P4Tag2::k_sndbuf).toInt();
	m_protocolRcvBufSize = msg->value(P4Tag2::k_rcvbuf).toInt();
}

void P4ConnectionPrivate::messageCryptoHandler(P4Message* inMsg)
{
	Q_Q(P4Connection);

	P4Message msg;
	QString dest(QString("%1:%2").arg(q->host()).arg(q->port()));

	QCryptographicHash md5_a(QCryptographicHash::Md5);
	QString token(inMsg->value(P4Tag2::k_token));

	QByteArray ticket = getTicket(inMsg->value(P4Tag2::k_serverAddress),
								  inMsg->value(P4Tag2::k_user));
	md5_a.addData(token.toLatin1());
	md5_a.addData(ticket);
	QByteArray res_a(md5_a.result().toHex().toUpper());

	QCryptographicHash md5_b(QCryptographicHash::Md5);
	md5_b.addData(res_a);
	md5_b.addData(dest.toLatin1());
	QByteArray res_b(md5_b.result().toHex().toUpper());
	INF("md5:" << res_b);

	msg.appendItem(P4Tag2::k_daddr, dest);
	msg.appendItem(P4Tag2::k_token, res_b);
	msg.appendItem(P4Tag2::k_func, P4Tag2::v_crypto);

	m_transport->send(msg);
}

void P4ConnectionPrivate::messageFlush1Handler(P4Message* inMsg)
{
		P4Message msg;
		msg.appendItem(P4Tag2::k_fseq, inMsg->value(P4Tag2::k_fseq));
		msg.appendItem(P4Tag2::k_himark, inMsg->value(P4Tag2::k_himark));
		msg.appendItem(P4Tag2::k_func, P4Tag2::v_flush2);

		m_transport->send(msg);
}

QByteArray P4ConnectionPrivate::getTicket(QString id, QString user)
{
	QString fileName(".p4tickets"); // FIXME don't embeed strings in source code
	QString filePath(QDir::home().absoluteFilePath(fileName));
	QFile file;
	file.setFileName(filePath);

	if (file.open(QFile::ReadOnly))
	{
		QByteArray ba(file.readAll());
		QList<QByteArray> lines(ba.split('\n'));
		foreach (QByteArray line, lines)
		{
			QList<QByteArray> arr(line.split('='));
			if (arr.count() > 1 &&
				arr.at(0) == id &&
				arr.at(1).startsWith(user.toUtf8()))
			{
				QByteArray ticket(arr.at(1));
				ticket.remove(0, user.length()+1);
				return ticket;
			}
			else
			{
				ERR(QString("ticket(%1,%2) invalid %3: %4")
					.arg(id).arg(user)
					.arg(filePath).arg(QString(line)));
			}
		}
	}
	else
	{
		ERR("Failed to open tickets file:" << fileName << file.errorString());
	}

	return QByteArray();
}

void P4ConnectionPrivate::on_transportConnected()
{
	Q_Q(P4Connection);
	emit q->connected();
}

void P4ConnectionPrivate::on_transportDisconnected(P4Result res)
{
	Q_Q(P4Connection);
	emit q->disconnected(res);
}

void P4ConnectionPrivate::on_messageReady()
{
	Q_Q(P4Connection);

	QList<P4Message*> msgList = m_transport->getAllMessages();
	foreach (P4Message *msg, msgList)
	{
		// Parse reply message
		// try to handle core messages
		if (msg->value(P4Tag2::k_func) == P4Tag2::v_protocol)
		{
			messageProtocolHandler(msg);
		}
		else if (msg->value(P4Tag2::k_func) == P4Tag2::v_release)
		{
			msg->removeItem(P4Tag2::k_func);
			m_pendingRequest->d_ptr->finished();
			emit q->requestFinished(m_pendingRequest);
			m_pendingRequest.clear();
		}
		else if (msg->value(P4Tag2::k_func) == P4Tag2::v_clientCrypto)
		{
			messageCryptoHandler(msg);
		}
		else if (msg->value(P4Tag2::k_func) == P4Tag2::v_flush1)
		{
			messageFlush1Handler(msg);
		}
		else
		{
			// try to handle message by request implementation
			// if not then we need add implementation to handle it or there is
			// some communication issue
			bool handled = m_pendingRequest->d_ptr->on_newMessage(msg);
			if (!handled)
			{
				QString inf(QString("Reply %1 not handled by %2")
							.arg(msg->value(P4Tag2::k_func))
							.arg(m_pendingRequest->metaObject()->className()));
				m_pendingRequest->d_ptr->appendResult(inf, P4RESULT_ERROR);
			}
		}
	}
}
