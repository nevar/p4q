#ifndef P4CONNECTION_P_H
#define P4CONNECTION_P_H

#include "p4q/P4Transport.h"
#include "p4q/request/P4Request.h"
#include <QtCore/QObject>
#include <QtCore/QSharedPointer>

class P4Connection;

/**
 * @brief The P4ConnectionPrivate class This class is not thread safe.
 */
class P4ConnectionPrivate : public QObject
{
	Q_OBJECT
	Q_DECLARE_PUBLIC(P4Connection)
	friend class P4Connection;

	P4ConnectionPrivate(P4Connection* q);

	P4Message newMsgProtocol() const;
	void messageProtocolHandler(P4Message *msg);
	void messageCryptoHandler(P4Message *inMsg);
	void messageFlush1Handler(P4Message *inMsg);
	static QByteArray getTicket(QString id, QString user);

private slots:
	void on_transportConnected();
	void on_transportDisconnected(P4Result res);
	void on_messageReady();

private:
	// received on init message
	int m_protocolProxy;
	int m_protocolBroker;
	int m_protocolXFiles;
	int m_protocolServer;
	int m_protocolServer2;
	QString m_protocolServerId;
	int m_protocolRevVer;
	int m_protocolTZOffset;
	int m_protocolSecurity;
	int m_protocolSndBufSize;
	int m_protocolRcvBufSize;

	QString m_defaultUser;
	P4Connection* q_ptr{nullptr};
	P4Transport *m_transport{nullptr};
	QSharedPointer<P4Request> m_pendingRequest;
	bool m_isPendingAsync{false};
};

#endif // P4CONNECTION_P_H