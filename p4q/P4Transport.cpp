/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "p4q/data/P4Result_p.h"
#include "p4q/P4Transport_p.h"
#include "p4q/data/P4Message_p.h"
#include "p4q/P4Transport.h"
#include "p4q/request/P4Tag.h"
#include "p4q/Log.h"
#include <sys/time.h>
#include <QtCore/QtGlobal>
#include <QtCore/QMetaEnum>
#include <QtNetwork/QHostAddress>

P4Transport::P4Transport(QObject *parent)
	: QObject(parent)
	, d_ptr(new P4TransportPrivate(this))
{
}

P4Transport::~P4Transport()
{
	Q_D(P4Transport);

	if (d->m_socket)
	{
		d->m_socket->disconnectFromHost();
		d->m_socket->deleteLater();
	}
}

P4Result P4Transport::send(const P4Message& msg)
{
	Q_D(P4Transport);

	P4Result res;
	QMutexLocker locker(&d->m_mutex);

	if (!isConnected())
	{
		res = P4Result("You need to connectToHost before you send any message", P4RESULT_ERROR);
		ERR(res.toString());
		return res;
	}

	if (!canSend())
	{
		res = P4Result("You need to wait for current message response befere sending new message.", P4RESULT_ERROR);
		ERR(res.toString());
		return res;
	}

	QByteArray msgData = msg.constructData();

#ifdef P4_MSG_DEBUG

	QString fname("unknown");
	if (msg.containsKey("func"))
		fname = msg.value("func");

	if (fname == d->m_func && d->m_io == "out")
	{
		++d->m_count;
		if ( d->m_count == 10 ||
			 d->m_count == 100 ||
			 d->m_count == 1000 ||
			 d->m_count == 10000 ||
			 d->m_count == 100000)
		{
			SIO("OUT func:" << d->m_func << " c:" << d->m_count);
		}
	}
	else
	{
		if ( d->m_count == 1 ||
			 d->m_count == 10 ||
			 d->m_count == 100 ||
			 d->m_count == 1000 ||
			 d->m_count == 10000 ||
			 d->m_count == 100000)
		{
		}
		else
		{
			if (d->m_io == "out")
				SIO("OUT func:" << d->m_func << " c:" << d->m_count);
			else
				SIO("IN  func:" << d->m_func << " c:" << d->m_count);
		}
		SIO("OUT func:" << fname << " c:" << 1);

		d->m_func = fname;
		d->m_io = "out";
		d->m_count = 1;
	}
#endif

	if (d->m_isOutputLogged)
	{
		Log log;
		LOG_COMMAND(log, &msg);
		if (Log::isPrintable(P4Config::LOG_TAG_MESSAGE) ||
			Log::isPrintable(P4Config::LOG_TAG_TRANSPORT) )
		{
			log << "===> " << objectName() << " OUT " << d->m_messagesSent << "\n";
			LOG_MESSAGE(log, &msg);
			LOG_TRANSPORT(log, msgData);
		}
	}

	if (msg.isResponseNeeded())
	{
		// block sending until we parse all response messages
		d->m_canSend = false;
	}

	qint64 count = d->m_socket->write(msgData);

	d->m_bytesSent += count;
	d->m_messagesSent++;

#ifdef P4_TRANS_DEBUG
	P4_TRANS("" << msgData);
	P4_TRANS("OUT" << m_messagesSent);
	foreach (P4Pair p, msg.list()) {
		P4_TRANS("    " << p.first << "\t" << p.second);
	}
#endif

	return res;
}

bool P4Transport::waitForMessages(int timeout)
{
	Q_D(P4Transport);

	QMutexLocker lock(&d->m_mutex);
	if (d->m_canSend)
		return true;

	bool isNewData = d->m_socket->waitForReadyRead(timeout);
	if (isNewData)
	{
		d->on_readyRead();
	}

	if (d->m_waitingMessaeges.count())
	{
		return true;
	}

	return false;
}

int P4Transport::dropWaitingMessages()
{
	Q_D(P4Transport);

	// FIXME we need to free all messages
	QMutexLocker lock(&d->m_mutex);
	int i = d->m_waitingMessaeges.count();
	d->m_waitingMessaeges.clear();
	return i;
}

int P4Transport::messagesSentCount() const
{
	Q_D(const P4Transport);

	QMutexLocker lock(&d->m_mutex);
	return d->m_messagesSent;
}

bool P4Transport::canSend() const
{
	Q_D(const P4Transport);

	QMutexLocker lock(&d->m_mutex);
	return d->m_canSend;
}

QString P4Transport::host() const
{
	Q_D(const P4Transport);

	QMutexLocker lock(&d->m_mutex);
	return d->m_socket->peerName();
}

qint16 P4Transport::port() const
{
	Q_D(const P4Transport);

	QMutexLocker lock(&d->m_mutex);
	return d->m_socket->peerPort();
}

QList<P4Message*> P4Transport::getAllMessages()
{
	Q_D(P4Transport);

	QMutexLocker lock(&d->m_mutex);
	// TODO can we optimize this?
	QList<P4Message*> l = d->m_waitingMessaeges;
	d->m_waitingMessaeges.clear();
	return l;
}

void P4Transport::showMessagesInLog(bool logInputMessages, bool logOutputMessages)
{
	Q_D(P4Transport);

	QMutexLocker lock(&d->m_mutex);
	d->m_isInputLogged = logInputMessages;
	d->m_isOutputLogged = logOutputMessages;
}

void P4Transport::connectToHostAsync(const QString& host, quint16 port)
{
	Q_D(P4Transport);

	QMutexLocker locker(&d->m_mutex);

	if (!d->m_socket)
	{
		setSocket(new QTcpSocket());
	}

	if ( isConnected() &&
		 d->m_socket->peerName() == host &&
		 d->m_socket->peerPort() == port )
	{
		return;
	}

	d->m_socket->connectToHost(host, port);
}

P4Result P4Transport::connectToHostSync(const QString& host, quint16 port, int timeout)
{
	Q_D(P4Transport);

	QMutexLocker locker(&d->m_mutex);
	if (!d->m_socket)
	{
		setSocket(new QTcpSocket());
	}

	if ( isConnected() &&
		 d->m_socket->peerName() == host &&
		 d->m_socket->peerPort() == port )
	{
		return P4Result();
	}

	d->m_socket->connectToHost(host, port);
	bool isConnected = d->m_socket->waitForConnected(timeout);
	if (isConnected)
	{
		INF(QString("Connected to %1:%2").arg(host).arg(QString::number(port)));
		return P4Result();
	}

	return P4Result(d->m_socket->errorString(), P4RESULT_ERROR);
}

void P4Transport::disconnectFromHost()
{
	Q_D(P4Transport);

	QMutexLocker locker(&d->m_mutex);
	if (d->m_socket->state() != QAbstractSocket::UnconnectedState)
	{
		d->m_socket->disconnectFromHost();
	}
}

P4Result P4Transport::disconnectFromHostSync(int timeout)
{
	Q_D(P4Transport);

	QMutexLocker locker(&d->m_mutex);

	if (!isDisconnected())
	{
		d->m_socket->disconnectFromHost();
		bool isDisconnected = d->m_socket->waitForDisconnected(timeout);
		if (!isDisconnected)
		{
			return P4Result(d->m_socket->errorString(), P4RESULT_ERROR);
		}
	}

	return P4Result();
}

// TODO Check if this function is used/needed by any use case
void P4Transport::setSocket(QTcpSocket* socket)
{
	Q_D(P4Transport);
	Q_ASSERT(socket);

	QMutexLocker locker(&d->m_mutex);
	if (d->m_socket == socket)
		return;

	if (d->m_socket)
	{
		d->m_socket->deleteLater();
		d->m_socket = nullptr;
		d->m_isConnected = false;
	}

	QObject::connect(socket, &QTcpSocket::readyRead,
					 d, &P4TransportPrivate::on_readyRead);
	QObject::connect(socket, &QTcpSocket::connected,
					 d, &P4TransportPrivate::on_socketConnected);
	QObject::connect(socket, &QTcpSocket::disconnected,
					 d, &P4TransportPrivate::on_socketDisconnected);
	QObject::connect(socket, SIGNAL(error(QAbstractSocket::SocketError)),
					 d, SLOT(on_socketError(QAbstractSocket::SocketError)));

	d->m_socket = socket;
	d->m_isConnected = socket->state() == QAbstractSocket::ConnectedState;
}

bool P4Transport::isConnected() const
{
	Q_D(const P4Transport);

	QMutexLocker locker(&d->m_mutex);
	return d->m_socket->state() == QAbstractSocket::ConnectedState;
}

bool P4Transport::isDisconnected() const
{
	Q_D(const P4Transport);

	QMutexLocker locker(&d->m_mutex);
	return d->m_socket->state() == QAbstractSocket::UnconnectedState;
}
