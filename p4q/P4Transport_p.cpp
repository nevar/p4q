#include "P4Transport_p.h"
#include "p4q/data/P4Message_p.h"
#include "p4q/Log.h"

P4TransportPrivate::P4TransportPrivate(P4Transport* q)
	: QObject(nullptr)
	, q_ptr(q)
{

}

bool P4TransportPrivate::processMessage()
{
	Q_Q(P4Transport);

	P4Message *msg = P4MessagePrivate::createMessage(m_rcvBuf);
	if (msg)
	{
#ifdef P4_TRANS_DEBUG
		P4_TRANS("IN" << m_messagesReceived);
		foreach (P4Pair p, msg->list()) {
			if (p.first == "code0")
			{
				P4_TRANS("    " << p.first << "\t" << ERROR_ID(p.second.toInt()));
			}
			else
			{
				P4_TRANS("    " << p.first << "\t" << p.second);
			}
		}
#endif

#ifdef P4_MSG_DEBUG
	QString fname("unknown");
	if (msg->containsKey("func"))
		fname = msg->value("func");

	if (fname == m_func && m_io == "in")
	{
		++m_count;
		if ( m_count == 10 ||
			 m_count == 100 ||
			 m_count == 1000 ||
			 m_count == 10000 ||
			 m_count == 100000)
		{
			SIO("IN  func:" << m_func << " c:" << m_count);
		}
	}
	else
	{
		if ( m_count == 1 ||
			 m_count == 10 ||
			 m_count == 100 ||
			 m_count == 1000 ||
			 m_count == 10000 ||
			 m_count == 100000)
		{
		}
		else
		{
			if (m_io == "out")
				SIO("OUT func:" << m_func << " c:" << m_count);
			else
				SIO("IN  func:" << m_func << " c:" << m_count);
		}
		SIO("IN  func:" << fname << " c:" << 1);

		m_func = fname;
		m_io = "in";
		m_count = 1;
	}
#endif

		if (m_isInputLogged)
		{
			QByteArray msgData = msg->constructData();
			Log log;
			LOG_COMMAND(log, msg);
			if (Log::isPrintable(P4Config::LOG_TAG_MESSAGE) ||
				Log::isPrintable(P4Config::LOG_TAG_TRANSPORT) )
			{
				log << "===> " << objectName() << " IN " << m_messagesSent << "\n";
				LOG_MESSAGE(log, msg);
				LOG_TRANSPORT(log, msgData);
			}
		}

		m_messagesReceived++;
		m_waitingMessaeges.prepend(msg);
		if (msg->containsKey("func") && msg->value("func") == "release")
		{
			m_canSend = true;
			emit q->transmissionFinished();
		}

		// TODO notify the waiting sync API?
		emit q->messagesReadyRead();
		return true;
	}

	return false;
}

void P4TransportPrivate::on_socketConnected()
{
	Q_Q(P4Transport);
	INF(QString("Connected to %1:%2").arg(m_socket->peerName()).arg(QString::number(m_socket->peerPort())));

	m_isConnected = true;
	m_canSend = true;
	emit q->connectedToHost();
}

void P4TransportPrivate::on_socketDisconnected()
{
	Q_Q(P4Transport);
	INF(QString("Disconnected from %1:%2").arg(m_socket->peerName()).arg(QString::number(m_socket->peerPort())));

	m_isConnected = false;
	m_canSend = false;
	emit q->disconnectedFromHost(P4Result());
}

void P4TransportPrivate::on_socketError(QAbstractSocket::SocketError socketError)
{
	Q_Q(P4Transport);

	P4Result res;
	const QMetaObject &mo = QAbstractSocket::staticMetaObject;
	int index = mo.indexOfEnumerator("SocketError");
	QMetaEnum me = mo.enumerator(index);
	res.appendResult(me.valueToKey(socketError), P4RESULT_ERROR);
	res.appendResult(m_socket->errorString(), P4RESULT_ERROR);
	emit q->disconnectedFromHost(res);
}

void P4TransportPrivate::on_readyRead()
{
	Q_ASSERT(m_socket);

	m_rcvBuf.append(m_socket->readAll());
	while (processMessage()) {}
}
