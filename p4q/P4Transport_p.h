#ifndef P4TRANSPORT_P_H
#define P4TRANSPORT_P_H

#include "p4q/P4Transport.h"

class P4TransportPrivate : public QObject
{
	Q_OBJECT
	Q_DECLARE_PUBLIC(P4Transport)
	Q_DISABLE_COPY(P4TransportPrivate)

	P4TransportPrivate(P4Transport *q);
	bool processMessage();

private slots:
	// socket signals
	void on_socketConnected();
	void on_socketDisconnected();
	void on_socketError(QAbstractSocket::SocketError socketError);
	void on_readyRead();

private:
	QTcpSocket *m_socket{nullptr};
	QByteArray m_rcvBuf;

	qlonglong m_bytesSent{0};
	qlonglong m_bytesReceived{0};
	uint m_messagesSent{0};
	uint m_messagesReceived{0};
	QList<P4Message*> m_waitingMessaeges;

	// values used by other thread
	mutable QMutex m_mutex{QMutex::Recursive};

	// TODO remove this
	bool m_isConnected;

	bool m_isInputLogged{true};
	bool m_isOutputLogged{true};

	bool m_canSend{false};

#ifdef P4_MSG_DEBUG // Used only when debugging enabled
	QString m_io;
	QString m_func;
	int m_count{0};
#endif

	P4Transport* q_ptr{nullptr};
};

#endif // P4TRANSPORT_P_H