/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef P4CLIENT_H
#define P4CLIENT_H

#include <p4q/data/P4Result.h>
#include <p4q/P4Global.h>
#include <QtCore/QObject>
#include <QtCore/QSharedPointer>

class P4ClientPrivate;
class P4Request;

/**
 * @brief The P4Client class used as a manager of requests sent to the P4 server.
 * Requests sent with use of client are executed simultaniously in muliply threads.
 */
class P4QSHARED_EXPORT P4Client : public QObject
{
	Q_OBJECT
	Q_DECLARE_PRIVATE(P4Client)

public:
	P4Client(QObject *parent = nullptr);
	Q_DISABLE_COPY(P4Client)

public:
	/**
	 * @brief send Send P4 request. Request can be delayed if all threads are busy.
	 * In such case request are inserted on the waiting queue and will be sent in the future.
	 * @param req Request object. Client gets full ownership of all requests.
	 * @param user User name to be used when sending request.
	 */
	void send(QSharedPointer<P4Request> req,
			  const QString& user = QString(),
			  const QString& workspace = QString());

	/**
	 * @brief host Current hostname
	 * @return
	 */
	QString host() const;

	/**
	 * @brief port Current port
	 * @return
	 */
	qint16 port() const;

	/**
	 * @brief setDefaultUser Set user to be used as default when sending requests
	 * @param aUser
	 */
	void setDefaultUser(QString aUser);

	/**
	 * @brief defaultUser
	 * @return Returns default user value.
	 */
	QString defaultUser() const;

	/**
	 * @brief setConnectionInfo Set new host and port to be used when sending requests.
	 * @param aHost Hostname or IP address
	 * @param aPort Port number
	 */
	void setConnectionInfo(const QString& aHost, qint16 aPort);

	/**
	 * @brief abort Cancell all connections and pending requests
	 */
	void abort();

signals:
	/**
	 * @brief stateChanged Emitted when client internal stat is changed.
	 * This information can be used to monitor how client is busy sending on
	 * requests.
	 * @param requestsWaiting Number of requests waiting in queue.
	 * @param idleConnetions Number of idle connections.
	 * @param busyConnections Number of busy connections.
	 */
	void stateChanged(int requestsWaiting, int idleConnetions, int busyConnections) const;

	/**
	 * @brief started Emited after request has been sent.
	 * @param cmd Full command string with arguments of sent request.
	 */
	void requestSent(QSharedPointer<P4Request> req);

	/**
	 * @brief finished Emited after request execution finishes.
	 * @param cmd Full command string with arguments of finished request.
	 * @param res Result object to check command fininsh status
	 * @param msDuration Duration time in ms
	 */
	void requestFinished(QSharedPointer<P4Request> req, int msDuration);

	/**
	 * @brief error Emited when there is any error when sending requests or
	 * connecting to server
	 * @param res Result object
	 */
	void requestError(QSharedPointer<P4Request> req);

private:
	P4ClientPrivate *d_ptr;
};

#endif // P4CLIENT_H
