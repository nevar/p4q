#ifndef P4CONFIG_H
#define P4CONFIG_H

namespace P4Config
{
	enum LogTag
	{
		LOG_TAG_INFO,
		LOG_TAG_DEBUG,
		LOG_TAG_ERROR,
		LOG_TAG_COMMAND,
		LOG_TAG_SHORT_INPUT_OUTPUT,
		LOG_TAG_MESSAGE,
		LOG_TAG_TRANSPORT,
		LOG_TAG_ALL,
	};

	void addLogTag(P4Config::LogTag tag);
	void removeLogTag(P4Config::LogTag tag);
	void removeAllLogTags();
}

#endif // P4CONFIG_H
