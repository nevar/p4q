/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef P4CONNECTION_H
#define P4CONNECTION_H

#include <p4q/data/P4Result.h>
#include <p4q/P4Global.h>
#include <QtCore/QObject>

class P4ConnectionPrivate;
class P4Request;

/**
 * @brief The P4Protocol class defines "protocol" how messages are send by socket.
 * Protocol defines how & when additional arguments are added to the message
 * before sending it to server.
 */
class P4QSHARED_EXPORT P4Connection : public QObject
{
	Q_OBJECT
	Q_DECLARE_PRIVATE(P4Connection)

public:
	P4Connection(QObject *parent = nullptr);
	virtual ~P4Connection();

signals:
	/**
	 * @brief connected Sent when connection to server was established.
	 */
	void connected();

	/**
	 * @brief disconnected Sent when connection with server was dropped.
	 * @param res Result with error or empty if no error occured.
	 */
	void disconnected(P4Result res);

	/**
	 * @brief requestFinished Sent when request finished with error or success.
	 */
	void requestFinished(QSharedPointer<P4Request>);

public:
	/**
	 * @brief setDefaultUser Set user to be used as default when sending requests
	 * @param aUser
	 */
	void setDefaultUser(QString aUser);

	/**
	 * @brief getDefaultUser Returns default user value.
	 * @return
	 */
	QString getDefaultUser() const;

	/**
	 * @brief connectToHost Connects to the host and port. If connection is established
	 * connected signal is emitted.
	 * @param host Server host
	 * @param port Server port
	 * @return
	 */
	P4Result connectToHost(QString host, quint16 port, int timeout = 5000);

	/**
	 * @brief disconnectFromHost Disconnects from p4 server
	 * @param timeout
	 * @return
	 */
	P4Result disconnectFromHost(int timeout = 5000);

	/**
	 * @brief isConnected Return current connection status.
	 * @return true if connected to the server.
	 */
	bool isConnected() const;

	/**
	 * @brief send Send P4 request. This is asynchronous function call.
	 * Reply will be reported by P4Request or P4Connection signals.
	 * @param req Request object.
	 */
	void send(QSharedPointer<P4Request> req);

	/**
	 * @brief sendAsync Blocking version of sending request.
	 * @param req
	 */
	void sendSync(QSharedPointer<P4Request> req);

	/**
	 * @brief host Return host used on connecting to server.
	 * @return Host.
	 */
	QString host() const;

	/**
	 * @brief port Returns port used to connect to server.
	 * @return Port number.
	 */
	qint16 port() const;

private:
	P4ConnectionPrivate *d_ptr;
};

#endif // P4CONNECTION_H
