/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef P4TRANSPORT_H
#define P4TRANSPORT_H

#include "p4q/data/P4Message.h"
#include "p4q/data/P4Result.h"
#include <QtCore/QObject>
#include <QtCore/QByteArray>
#include <QtCore/QThread>
#include <QtNetwork/QTcpSocket>
#include <QtCore/QMutex>
#include <p4q/P4Global.h>

class P4TransportPrivate;

/**
 * @brief The P4Transport class This class is not thread safe and should be used
 * only from one thread.
 */
// FIXME check if mutex is locked when needed
// functions used by other thread? this class should be thread safe?
// TODO add doxygen
class P4QSHARED_EXPORT P4Transport : public QObject
{
	Q_OBJECT
	Q_DECLARE_PRIVATE(P4Transport)
	Q_DISABLE_COPY(P4Transport)

public:
	P4Transport(QObject* parent = nullptr);
	~P4Transport();

public slots:
	/**
	 * @brief dropWaitingMessages Drops all messages already received.
	 * @return
	 */
	int dropWaitingMessages();

	/**
	 * @brief getAllMessages Returns messages received from last getAllMessages
	 * call. Caller is responsible for freeing all message pointers.
	 * @return
	 */
	QList<P4Message*> getAllMessages();

	/**
	 * @brief showMessagesInLog This is used only for debugging multithreaded
	 * applications when we have many P4Transport instances. Then we can select
	 * which should output debug logs.
	 * @param logInputMessages
	 * @param logOutputMessages
	 */
	void showMessagesInLog(bool logInputMessages, bool logOutputMessages);

	/**
	 * @brief connectToHostAsync Connect to p4 server host:port
	 * @param host
	 * @param port
	 */
	void connectToHostAsync(const QString& host, quint16 port);

	/**
	 * @brief connectToHost Connect to p4 server host:port in sync blocking mode
	 * @param host
	 * @param port
	 * @param timeout
	 * @return
	 */
	P4Result connectToHostSync(const QString& host, quint16 port, int timeout = 5000);

	/**
	 * @brief disconnectFromHostAsync Disconnect from p4 server
	 */
	void disconnectFromHost();

	/**
	 * @brief disconnectFromHost Disconnect current connection in sync blocking mode.
	 * @param timeout
	 * @return
	 */
	P4Result disconnectFromHostSync(int timeout = 5000);

	/**
	 * @brief host Return hostname used to establish connection with server.
	 * @return
	 */
	QString host() const;

	/**
	 * @brief port Return port number used to establish connection with server.
	 * @return
	 */
	qint16 port() const;

	/**
	 * @brief isConnected Return true when socket is connected to the server.
	 * @return
	 */
	bool isConnected() const;

	/**
	 * @brief isDisconnected Return true when socket is in disconnected state.
	 * @return
	 */
	bool isDisconnected() const;

	/**
	 * @brief setSocket Set new internal socket for communicating with p4 server.
	 * @param socket
	 */
	void setSocket(QTcpSocket *socket);

	/**
	 * @brief send Send message to the server.
	 * @param msg
	 * @return
	 */
	P4Result send(const P4Message& msg);

	/**
	 * @brief waitForMessages Wait for messages from server
	 * @param timeout
	 * @return
	 */
	bool waitForMessages(int timeout = 5000);

	/**
	 * @brief messagesSentCount Returns number of messages sent with this transport
	 * object from the beginning of object creation.
	 * @return
	 */
	int messagesSentCount() const;

	/**
	 * @brief canSend Determines if transmission is in output/send mode (canSend() == true)
	 * or in input/receive mode (canSend() == false)
	 * @return
	 */
	bool canSend() const;

signals:
	void messagesReadyRead();

	/**
	 * @brief transmissionFinished Send only on those messages which receive
	 * reply from server
	 */
	void transmissionFinished();

	void connectedToHost();
	void disconnectedFromHost(P4Result res);

private:
	P4TransportPrivate* d_ptr{nullptr};
};

#endif // P4TRANSPORT_H