/*
 * Copyright (c) 2018 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef P4DATA_H
#define P4DATA_H

#include <p4q/P4Global.h>
#include <QtCore/QString>
#include <QtCore/QHash>

class P4DataPrivate;

class P4QSHARED_EXPORT P4Data
{
	Q_DECLARE_PRIVATE(P4Data)

public:
	P4Data();
	P4Data(const QHash<QString,QString> h);
	P4Data(const P4Data& data);
	P4Data& operator=(const P4Data& other);
	~P4Data();

	QHash<QString,QString> hash() const;
	QString value(const QString &key) const;

private:
	P4DataPrivate* d_ptr;
};

#endif // P4DATA_H