/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef P4MESSAGE_H
#define P4MESSAGE_H

#include <QtCore/QByteArray>
#include <QtCore/QHash>
#include <QtCore/QPair>
#include <p4q/P4Global.h>

class P4MessagePrivate;

typedef QPair<QString,QString> P4Pair;

class P4QSHARED_EXPORT P4Message
{
	Q_DECLARE_PRIVATE(P4Message)

public:
	P4Message();
	P4Message(QByteArray msgData);

	bool isResponseNeeded() const;

	QList<P4Pair> itemList() const;
	QHash<QString, QString> itemHash() const;
	int itemsCount() const;

	void appendItem(const QString& k, const QString& v);
	void removeItem(const QString& k);
	void clearItems();
	bool containsKey(const QString& key) const;
	QString value(const QString& key) const;

	QByteArray constructData() const;

private:
	P4MessagePrivate* d_ptr{nullptr};
};

#endif // P4MESSAGE_H