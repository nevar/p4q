/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef P4RESULT_H
#define P4RESULT_H

#include <p4q/P4Global.h>
#include <QtCore/QString>

class P4ResultPrivate;

// Order is important
enum P4ResultType
{
	P4RESULT_INFO,
	P4RESULT_WARNING,
	P4RESULT_ERROR,
};

/**
 * @brief The P4Result is class representing status of finished P4 command.
 */
class P4QSHARED_EXPORT P4Result
{
	Q_DECLARE_PRIVATE(P4Result)

public:
	P4Result();
	P4Result(QString msg, P4ResultType type);
	P4Result(const P4Result &other);
	P4Result& operator=(const P4Result &other);
	~P4Result();

	/**
	 * @brief reset Clear all result info
	 */
	void reset();

public:
	/**
	 * @brief appendResult Append result type and message to object
	 * @param msg String message
	 * @param type Type of message
	 */
	void appendResult(QString msg, P4ResultType type);

	/**
	 * @brief isError Is result was on error
	 * @return
	 */
	bool isError() const;

	/**
	 * @brief isEmpty Is result empty (result without any output information)
	 * @return
	 */
	bool isEmpty() const;

	/**
	 * @brief toString Get result string
	 * @return
	 */
	QString toString() const;

	/**
	 * @brief operator QString OString Operator which can be used with qDebug()
	 */
	operator QString() const;

private:
	P4ResultPrivate* d_ptr;
};

#endif // P4RESULT_H
