/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef P4REQUEST_H
#define P4REQUEST_H

#include <p4q/data/P4Result.h>
#include <p4q/P4Client.h>
#include <p4q/P4Connection.h>
#include <p4q/P4Global.h>
#include <QtCore/QObject>

class P4RequestPrivate;

/**
 * @brief The P4Request class Base class for all requests sent/recexved to server.
 * This class is for internal use and there is no need for normal API user to
 * derive from it or create objects of its type.
 */
class P4QSHARED_EXPORT P4Request : public QObject
{
	Q_OBJECT
	Q_DECLARE_PRIVATE(P4Request)
	Q_DISABLE_COPY(P4Request)

	// to append result
	friend class P4ClientPrivate;

	// to send finished signal
	friend class P4ConnectionPrivate;

	// FIXME why?
	friend class P4Connection;

	// to get P4Request internal message data needed to sent it as a message
	// this data need to be generated in the scope of the P4Protocol object
	friend void P4Connection::send(QSharedPointer<P4Request> req);

protected:
	explicit P4Request(QObject *parent = nullptr);
	P4Request(P4RequestPrivate& d, QObject *parent);

public:
	virtual ~P4Request();

	// TODO Change name to commandName()
	/**
	 * @brief command Returns command string that will be used to send requent to server.
	 * @return Command string
	 */
	QString command() const;

	/**
	 * @brief fullCommand Returns full command string including arguments that
	 * will be used to send request to server.
	 * @return
	 */
	QString fullCommand() const;

	/**
	 * @brief getResult Returns result of finished request or empty result object
	 * for unfinished request.
	 * @return Result object.
	 */
	P4Result result() const;

	/**
	 * @brief setUser Set user used as owner in sending request to the server
	 * @param aUser
	 */
	void setUser(QString aUser);

	/**
	 * @brief user Gets user that will be used on sending request.
	 * @return
	 */
	QString user() const;

	/**
	 * @brief setWorkspace Set workspace used when sending request to the server
	 * @param aWorkspace
	 */
	void setWorkspace(QString aWorkspace);

	/**
	 * @brief workspace Gets workspace that will be used on sending request.
	 * @return
	 */
	QString workspace() const;

signals:
	// TODO change name to sent, remove argument
	/**
	 * @brief started Emited after command has been sent.
	 * @param cmd Full command string with arguments.
	 */
	void started(const QString cmd);

	// TODO there is no need to pass cmd and res arguments in signal
	/**
	 * @brief finished Emited after command execution finished.
	 * @param cmd Full command string with arguments.
	 * @param res Result object to check command fininsh status
	 * @param msDuration Duration time in ms
	 */
	void finished(const QString& cmd, const P4Result& res, int msDuration);

protected:
	P4RequestPrivate *d_ptr;
};

#endif // P4REQUEST_H
