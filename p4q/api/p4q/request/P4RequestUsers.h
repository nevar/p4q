/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef P4USERSREQUEST_H
#define P4USERSREQUEST_H

#include <p4q/request/P4Request.h>
#include <p4q/data/P4User.h>
#include <p4q/P4Global.h>
#include <QtCore/QObject>

class P4RequestUsersPrivate;

class P4QSHARED_EXPORT P4RequestUsers : public P4Request
{
	Q_OBJECT
	Q_DECLARE_PRIVATE(P4RequestUsers)
	Q_DISABLE_COPY(P4RequestUsers)

public:
	enum P4RequestUsersFlags
	{
		P4_REQUEST_NONE				= 0,
		P4_REQUEST_ALL				= 1,
		P4_REQUEST_REPLICA_MASTER	= 2,
		P4_REQUEST_REPLICA_CURRENT	= 4,
		P4_REQUEST_LOGIN_INFO		= 8,
	};

public:
	P4RequestUsers(QObject *parent = nullptr);
	~P4RequestUsers();

	void setFlags(P4RequestUsersFlags flags);
	void setFlag(P4RequestUsersFlags flags);
	void resetFlag(P4RequestUsersFlags flags);
	void setMax(int max);
	void setUser(QString user);

	QList<P4User> getUsers() const;

signals:
	void usersChanged(const QList<P4User> users);
};

#endif // P4USERSREQUEST_H
