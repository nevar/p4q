/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "p4q/data/P4Change.h"
#include "p4q/request/P4Tag.h"
#include <QtCore/QHash>

P4Change::P4Change()
{

}

P4Change::P4Change(const QHash<QString, QString> h)
	: P4Data(h)
{

}

long long P4Change::changeId() const
{
	QString str(hash().value(P4Tag2::k_change_id));
	return str.toLongLong();
}

QString P4Change::type() const
{
	return hash().value(P4Tag2::k_change_type);
}

QString P4Change::client() const
{
	return hash().value(P4Tag2::k_change_client);
}

QString P4Change::description() const
{
	return hash().value(P4Tag2::k_change_desc);
}

QString P4Change::owner() const
{
	return hash().value(P4Tag2::k_change_owner);
}

QString P4Change::path() const
{
	return hash().value(P4Tag2::k_change_path);
}

QString P4Change::status() const
{
	return hash().value(P4Tag2::k_change_status);
}

QDateTime P4Change::updateDate() const
{
	QString str(hash().value(P4Tag2::k_change_time));
	return QDateTime::fromTime_t(str.toLongLong());
}
