/*
 * Copyright (c) 2018 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "p4q/data/P4Data.h"
#include "P4Data_p.h"

P4Data::P4Data()
	: d_ptr(new P4DataPrivate(this))
{

}

P4Data::P4Data(const QHash<QString, QString> h)
	: P4Data()
{
	Q_D(P4Data);
	d->m_items = h;
}

P4Data::P4Data(const P4Data& data)
	: P4Data(data.d_ptr->m_items)
{
}

P4Data& P4Data::operator=(const P4Data& other)
{
	Q_D(P4Data);
	d->m_items = other.d_ptr->m_items;
	return *this;
}

P4Data::~P4Data()
{
	delete d_ptr;
}

QHash<QString, QString> P4Data::hash() const
{
	Q_D(const P4Data);
	return d->m_items;
}

QString P4Data::value(const QString& key) const
{
	Q_D(const P4Data);
	return d->m_items.value(key);
}
