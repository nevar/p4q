/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "P4Error.h"

int P4Error::subCode(int code)
{
	return (code >> 0) & 0x3ff;
}

int P4Error::subsystem(int code)
{
	return (code >> 10) & 0x3f;
}

int P4Error::generic(int code)
{
	return (code >> 16) & 0xff;
}

int P4Error::argCount(int code)
{
	return (code >> 24) & 0x0f;
}

P4Severity P4Error::severity(int code)
{
	return P4Severity((code >> 28) & 0x0f);
}

int P4Error::uniqueCode(int code)
{
	return code & 0xffff;
}

P4ResultType P4Error::resultType(int code)
{
	switch (severity(code)) {
	case P4ERROR_EMPTY:
	case P4ERROR_INFO:
		return P4RESULT_INFO;
	case P4ERROR_WARN:
		return P4RESULT_WARNING;
	case P4ERROR_FAILED:
	case P4ERROR_FATAL:
	default:
		return P4RESULT_ERROR;
	}
}

int P4Error::subCode(const QString& code)
{
	return subCode(code.toInt());
}

int P4Error::subsystem(const QString& code)
{
	return subsystem(code.toInt());
}

int P4Error::generic(const QString& code)
{
	return generic(code.toInt());
}

int P4Error::argCount(const QString& code)
{
	return argCount(code.toInt());
}

P4Severity P4Error::severity(const QString& code)
{
	return severity(code.toInt());
}

int P4Error::uniqueCode(const QString& code)
{
	return uniqueCode(code.toInt());
}

P4ResultType P4Error::resultType(const QString &code)
{
	return resultType(code.toInt());
}
