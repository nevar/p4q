/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef P4ERROR_H
#define P4ERROR_H

#include "p4q/data/P4Result.h"
#include <p4q/P4Global.h>

enum P4Severity
{
	P4ERROR_EMPTY = 0,
	P4ERROR_INFO = 1,
	P4ERROR_WARN = 2,
	P4ERROR_FAILED = 3,
	P4ERROR_FATAL = 4,
};

namespace P4Error
{
	int subCode(int code);
	int subsystem(int code);
	int generic(int code);
	int argCount(int code);
	P4Severity severity(int code);
	int uniqueCode(int code);
	P4ResultType resultType(int code);

	int subCode(const QString& code);
	int subsystem(const QString& code);
	int generic(const QString& code);
	int argCount(const QString& code);
	P4Severity severity(const QString& code);
	int uniqueCode(const QString& code);
	P4ResultType resultType(const QString& code);
}

// NOTE Parse this or remove if not needed
/*

enum ErrorGeneric {

	EV_NONE 	= 0,	// misc

	// The fault of the user

	EV_USAGE	= 0x01,	// request not consistent with dox
	EV_UNKNOWN	= 0x02,	// using unknown entity
	EV_CONTEXT	= 0x03,	// using entity in wrong context
	EV_ILLEGAL	= 0x04,	// trying to do something you can't
	EV_NOTYET	= 0x05,	// something must be corrected first
	EV_PROTECT	= 0x06,	// protections prevented operation

	// No fault at all

	EV_EMPTY	= 0x11,	// action returned empty results

	// not the fault of the user

	EV_FAULT	= 0x21,	// inexplicable program fault
	EV_CLIENT	= 0x22,	// client side program errors
	EV_ADMIN	= 0x23,	// server administrative action required
	EV_CONFIG	= 0x24,	// client configuration inadequate
	EV_UPGRADE	= 0x25,	// client or server too old to interact
	EV_COMM		= 0x26,	// communications error
	EV_TOOBIG	= 0x27	// not ever Perforce can handle this much

} ;

enum ErrorSubsystem {

	ES_OS		= 0,	// OS error
	ES_SUPP		= 1,	// Misc support
	ES_LBR		= 2,	// librarian
	ES_RPC		= 3,	// messaging
	ES_DB		= 4,	// database
	ES_DBSUPP	= 5,	// database support
	ES_DM		= 6,	// data manager
	ES_SERVER	= 7,	// top level of server
	ES_CLIENT	= 8,	// top level of client
	ES_INFO		= 9,	// pseudo subsystem for information messages
	ES_HELP		= 10,	// pseudo subsystem for help messages
	ES_SPEC		= 11,	// pseudo subsystem for spec/comment messages
	ES_FTPD		= 12,	// P4FTP server
	ES_BROKER	= 13,	// Perforce Broker
	ES_P4QT		= 14,	// P4V and other Qt based clients
	ES_X3SERVER	= 15	// P4X3 server
} ;
 */

#endif // P4ERROR_H
