/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "p4q/data/P4Message.h"
#include "p4q/request/P4Tag.h"
#include "p4q/data/P4Message_p.h"
#include "p4q/Log.h"
#include <QtCore/QtGlobal>

P4Message::P4Message()
	: d_ptr(new P4MessagePrivate(this))
{
}

P4Message::P4Message(QByteArray msgData)
	: d_ptr(new P4MessagePrivate(this))
{
	const char *charData = msgData.data();

	int i = P4MessagePrivate::headerSize;
	while(i < msgData.size())
	{
		QString key(QString::fromUtf8(&charData[i]));
		const char *dataPtr = &charData[i+key.toUtf8().size()];
		i += key.toUtf8().size()+5;
		if (i >= msgData.size()) break;

		quint32 *pLen = (quint32*)&dataPtr[1];
		QString v(QString::fromUtf8(&charData[i], *pLen));
		i += *pLen+1;

		appendItem(key, v);
	}
}

bool P4Message::isResponseNeeded() const
{
	Q_D(const P4Message);

	if (d->m_hash.contains(P4Tag2::k_func))
		if (d->m_hash.value(P4Tag2::k_func) == P4Tag2::u_users)
			return true;
	// TODO implement this
	return false;
}

QList<P4Pair> P4Message::itemList() const
{
	Q_D(const P4Message);
	return d->m_list;
}

QHash<QString, QString> P4Message::itemHash() const
{
	Q_D(const P4Message);
	return d->m_hash;
}

int P4Message::itemsCount() const
{
	Q_D(const P4Message);
	return d->m_hash.size();
}

void P4Message::appendItem(const QString& key, const QString& value)
{
	Q_D(P4Message);

	if (key != P4Tag2::k_empty && d->m_hash.contains(key))
	{
		d->m_hash[key] = value;
		QList<P4Pair>::iterator i = d->m_list.begin();
		while (i != d->m_list.end())
		{
			if ((*i).first == key)
			{
				(*i).second = value;
			}
			i++;
		}
		return;
	}

	d->m_hash.insert(key, value);
	d->m_list.append(P4Pair(key, value));
}

void P4Message::removeItem(const QString& k)
{
	Q_D(P4Message);

	d->m_hash.remove(k);

	QList<P4Pair>::iterator it = d->m_list.begin();
	while (it != d->m_list.end())
	{
		if ((*it).first == k)
		{
			it = d->m_list.erase(it);
			return;
		}
		else
			++it;
	}
}

void P4Message::clearItems()
{
	Q_D(P4Message);

	d->m_hash.clear();
	d->m_list.clear();
}

bool P4Message::containsKey(const QString& key) const
{
	Q_D(const P4Message);

	return d->m_hash.contains(key);
}

QString P4Message::value(const QString& key) const
{
	Q_D(const P4Message);

	return d->m_hash.value(key);
}

QByteArray P4Message::constructData() const
{
	Q_D(const P4Message);

	QByteArray msgData;
	msgData.resize(P4MessagePrivate::headerSize);
	foreach (P4Pair p, d->m_list)
	{
		QString k = p.first;
		QString v = p.second;
		msgData.append(k.toUtf8());
		msgData.append(static_cast<char>(0));
		qint32 vl = v.toUtf8().length();
		msgData.append((const char*)&vl, 4);
		if (!v.isEmpty())
			msgData.append(v.toUtf8());
		msgData.append(static_cast<char>(0));
	}

	char *dp = msgData.data();
	quint32 *pLen = (quint32*)&dp[1];
	*pLen = msgData.size() - P4MessagePrivate::headerSize;
	dp[0] = dp[1]^dp[2]^dp[3]^dp[4];

	return msgData;
}
