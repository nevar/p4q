#include "P4Message_p.h"
#include "p4q/Log.h"

P4MessagePrivate::P4MessagePrivate(P4Message* q)
	: q_ptr(q)
{

}

int P4MessagePrivate::getMessageContentSize(const QByteArray& msgData)
{
	if (msgData.size() < headerSize)
		return 0;

	const char *dp = msgData.data();
	return *(quint32*)&dp[1];
}

int P4MessagePrivate::headerOffset(const QByteArray& buf)
{
	if (buf.size() < headerSize)
		return -1;

	const char* dp = buf.constData();
	for(int i = 0; i <= buf.size() - headerSize; i++)
	{
		char sum = dp[1]^dp[2]^dp[3]^dp[4];
		if (dp[0] == sum)
		{
			return i;
		}
	}

	return -2;
}

P4Message* P4MessagePrivate::createMessage(QByteArray& rcvBuf)
{
	if (rcvBuf.isEmpty() || rcvBuf.size() < P4MessagePrivate::headerSize)
		return nullptr;

	int offset = headerOffset(rcvBuf);
	if (offset > 0)
	{
		// Message arrived but there are some unknown bytes before
		rcvBuf = rcvBuf.right(rcvBuf.size() - offset);
		ERR("Garbage bytes unknown protocol or server implementation" << offset);
	}
	else if (offset < 0)
	{
		ERR("Invalid offset value" << offset);
		return nullptr;
	}

	int msgSize = getMessageContentSize(rcvBuf);
	if (msgSize + P4MessagePrivate::headerSize <= rcvBuf.size())
	{
		QByteArray msgData = rcvBuf.left(msgSize + P4MessagePrivate::headerSize);

		rcvBuf = rcvBuf.right(rcvBuf.size() - P4MessagePrivate::headerSize - msgSize);
		P4Message *msg = new P4Message(msgData);
		return msg;
	}

	// Incomplete message need to wait for more data on next TCP message
	return nullptr;
}
