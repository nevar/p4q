#ifndef P4MESSAGE_P_H
#define P4MESSAGE_P_H

#include <QtCore/QtCore>
#include "p4q/data/P4Message.h"

class P4MessagePrivate
{
	Q_DECLARE_PUBLIC(P4Message)
	P4MessagePrivate(P4Message* q);

	static int getMessageContentSize(const QByteArray& msgData);
	static int headerOffset(const QByteArray& buf);

	static const int headerSize = 5;
	QHash<QString, QString> m_hash;
	QList<P4Pair> m_list;

	P4Message* q_ptr{nullptr};

public:
	static P4Message* createMessage(QByteArray& rcvBuf);
};

#endif // P4MESSAGE_P_H