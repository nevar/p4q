/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "p4q/data/P4Result.h"
#include "P4Result_p.h"

P4Result::P4Result()
	: d_ptr(new P4ResultPrivate(this))
{
	Q_D(P4Result);
	d->m_type = P4RESULT_INFO;
}

P4Result::P4Result(QString msg, P4ResultType type)
	: d_ptr(new P4ResultPrivate(this))
{
	Q_D(P4Result);
	appendResult(msg, type);
}

P4Result::P4Result(const P4Result& other)
	: d_ptr(new P4ResultPrivate(this, other.d_ptr))
{
}

P4Result::~P4Result()
{
	delete(d_ptr);
}

P4Result& P4Result::operator=(const P4Result& other)
{
	Q_D(P4Result);
	d->m_msg = other.d_ptr->m_msg;
	d->m_type = other.d_ptr->m_type;

	return *this;
}

bool P4Result::isError() const
{
	Q_D(const P4Result);

	if (d->m_type == P4RESULT_ERROR)
		return true;

	return false;
}

bool P4Result::isEmpty() const
{
	Q_D(const P4Result);
	return d->m_msg.isEmpty();
}

QString P4Result::toString() const
{
	Q_D(const P4Result);
	return d->m_msg;
}

void P4Result::reset()
{
	Q_D(P4Result);
	d->m_msg.clear();
	d->m_type = P4RESULT_INFO;
}

void P4Result::appendResult(QString msg, P4ResultType type)
{
	Q_D(P4Result);
	if (msg.isEmpty())
		return;

	QString prefix;

	switch (type) {
	case P4RESULT_INFO:
		prefix = "[INF]";
		break;
	case P4RESULT_WARNING:
		prefix = "[WRN]";
		break;
	case P4RESULT_ERROR:
	default:
		prefix = "[ERR]";
		break;
	}

	if (!d->m_msg.isEmpty())
	{
		prefix = QString("\n") + prefix;
	}

	d->m_msg += prefix + " " + msg;
	if (d->m_type < type)
		d->m_type = type;
}

P4Result::operator QString() const
{
	return toString();
}
