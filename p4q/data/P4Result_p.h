/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef P4RESULT_P_H
#define P4RESULT_P_H

#include "p4q/data/P4Result.h"
#include <QtCore/QString>

class P4Result;

class P4ResultPrivate
{
	Q_DECLARE_PUBLIC(P4Result)

	P4ResultPrivate(P4Result* q);
	P4ResultPrivate(P4Result* q, P4ResultPrivate* other);
	Q_DISABLE_COPY(P4ResultPrivate)
	~P4ResultPrivate();

	P4Result* q_ptr;
	QString m_msg;
	P4ResultType m_type;
};

#endif // P4RESULT_P_H
