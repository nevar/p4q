/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "p4q/data/P4User.h"
#include "p4q/request/P4Tag.h"
#include "p4q/Log.h"

P4User::P4User()
{
}

P4User::P4User(const QHash<QString, QString> h)
	: P4Data(h)
{
}

QString P4User::name() const
{
	return hash().value(P4Tag2::k_user_name);
}

QString P4User::fullName() const
{
	return hash().value(P4Tag2::k_user_fullName);
}

QString P4User::type() const
{
	return hash().value(P4Tag2::k_user_type);
}

QDateTime P4User::accessDate() const
{
	QString str(hash().value(P4Tag2::k_user_access));
	return QDateTime::fromTime_t(str.toLongLong());
}

QDateTime P4User::updateDate() const
{
	QString str(hash().value(P4Tag2::k_user_update));
	return QDateTime::fromTime_t(str.toLongLong());
}

bool P4User::isPassword() const
{
	QString sEnabled = hash().value(P4Tag2::k_user_password);
	bool isEnabled = false;
	if (sEnabled == "enabled")
		isEnabled = true;

	return isEnabled;
}

QString P4User::email() const
{
	return hash().value(P4Tag2::k_user_email);
}

QString P4User::jobsView() const
{
	return hash().value(P4Tag2::k_user_jobView);
}

QString P4User::authMethod() const
{
	return hash().value(P4Tag2::k_user_authMethod);
}

QString P4User::reviews() const
{
	return hash().value(P4Tag2::k_user_reviews);
}
