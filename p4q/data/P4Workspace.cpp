/*
 * Copyright (c) 2018 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "p4q/data/P4Workspace.h"
#include "p4q/request/P4Tag.h"

P4Workspace::P4Workspace()
{

}

P4Workspace::P4Workspace(const QHash<QString, QString> h)
	: P4Data(h)
{
}

QString P4Workspace::name() const
{
	return hash().value(P4Tag2::k_workspace_name);
}

QString P4Workspace::description() const
{
	return hash().value(P4Tag2::k_workspace_desc);
}

QDateTime P4Workspace::accessDate() const
{
	QString str(hash().value(P4Tag2::k_workspace_access));
	return QDateTime::fromTime_t(str.toLongLong());
}

QDateTime P4Workspace::updateDate() const
{
	QString str(hash().value(P4Tag2::k_workspace_update));
	return QDateTime::fromTime_t(str.toLongLong());
}

QString P4Workspace::host() const
{
	return hash().value(P4Tag2::k_workspace_host);
}

QString P4Workspace::type() const
{
	return hash().value(P4Tag2::k_workspace_type);
}

QString P4Workspace::options() const
{
	return hash().value(P4Tag2::k_workspace_options);
}

QString P4Workspace::owner() const
{
	return hash().value(P4Tag2::k_workspace_owner);
}

QString P4Workspace::root() const
{
	return hash().value(P4Tag2::k_workspace_root);
}

QStringList P4Workspace::altRoots() const
{
	QStringList ret;

	QString c(P4Tag2::k_workspace_altRoots);
	c.chop(1);
	foreach (QString key, hash().keys())
	{
		if (key.startsWith(c))
		{
			ret << hash().value(key);
		}
	}

	return ret;
}

QString P4Workspace::submitOptions() const
{
	return hash().value(P4Tag2::k_workspace_submitOptions);
}

QString P4Workspace::lineEnd() const
{
	return hash().value(P4Tag2::k_workspace_lineEnd);
}

QString P4Workspace::view() const
{
	return hash().value(P4Tag2::k_workspace_view);
}
