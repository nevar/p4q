/*
 * Copyright (c) 2018 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef P4WORKSPACE_H
#define P4WORKSPACE_H

#include <p4q/data/P4Data.h>
#include <p4q/P4Global.h>
#include <QtCore/QString>
#include <QtCore/QDateTime>

class P4QSHARED_EXPORT P4Workspace : public P4Data
{
public:
	P4Workspace();
	P4Workspace(const QHash<QString,QString> h);

	QString name() const;
	QString description() const;
	QDateTime accessDate() const;
	QDateTime updateDate() const;
	QString host() const;
	QString type() const;
	QString options() const;
	QString owner() const;
	QString root() const;
	QStringList altRoots() const;
	QString submitOptions() const;
	QString lineEnd() const;
	QString view() const;
};

#endif // P4WORKSPACE_H