/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "p4q/data/P4Result_p.h"
#include "p4q/request/P4Request_p.h"
#include "p4q/request/P4Request.h"
#include "p4q/Log.h"

P4Request::P4Request(QObject *parent)
	: QObject(parent)
	, d_ptr(new P4RequestPrivate(this))
{
}

P4Request::P4Request(P4RequestPrivate& d, QObject* parent)
	: QObject(parent)
	, d_ptr(&d)
{
	d.registerTypes();
}

P4Request::~P4Request()
{
	delete d_ptr;
}

QString P4Request::fullCommand() const
{
	Q_D(const P4Request);
	QString cmd = d->m_command + " ";
	cmd += d->getArgs().join(' ');
	return cmd;
}

P4Result P4Request::result() const
{
	Q_D(const P4Request);
	return d->m_result;
}

void P4Request::setUser(QString aUser)
{
	Q_D(P4Request);
	d->setUser(aUser);
}

QString P4Request::user() const
{
	Q_D(const P4Request);
	return d->user();
}

void P4Request::setWorkspace(QString aWorkspace)
{
	Q_D(P4Request);
	d->setWorkspace(aWorkspace);
}

QString P4Request::workspace() const
{
	Q_D(const P4Request);
	return d->workspace();
}

QString P4Request::command() const
{
	Q_D(const P4Request);
	return d->m_command;
}
