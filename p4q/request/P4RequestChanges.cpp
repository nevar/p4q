/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "P4RequestChanges.h"
#include "P4RequestChanges_p.h"

P4RequestChanges::P4RequestChanges(const QString& path, const QString& user)
	: P4Request(* new P4RequestChangesPrivate(this), nullptr)
{
	Q_D(P4RequestChanges);

	d->setCommand("changes");
	d->m_searchPath = path;
	d->m_user = user;
}

void P4RequestChanges::setStatusFilter(P4RequestChanges::ChangeStatus status)
{
	Q_D(P4RequestChanges);

	d->m_status = status;
}
