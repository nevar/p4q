/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "p4q/request/P4RequestChanges_p.h"
#include "p4q/request/P4Tag.h"
#include <QtCore/QMetaEnum>

P4RequestChangesPrivate::P4RequestChangesPrivate(P4RequestChanges* q)
	: P4RequestPrivate(q)
	, m_status(P4RequestChanges::NONE)
{

}

bool P4RequestChangesPrivate::on_newMessage(P4Message* msg)
{
	bool handled = P4RequestPrivate::on_newMessage(msg);
	if (handled)
		return true;

	if (msg->itemHash().value(P4Tag2::k_func) == P4Tag2::v_clientFstatInfo)
	{
		m_response.append(P4Change(msg->itemHash()));
		return true;
	}

	return false;
}

void P4RequestChangesPrivate::finished()
{
	Q_Q(P4RequestChanges);
	P4RequestPrivate::finished();
	emit q->changes(m_searchPath, m_user, m_response);
}

void P4RequestChangesPrivate::started()
{
	m_response.clear();
}

QStringList P4RequestChangesPrivate::getArgs() const
{
	QStringList s;
	if (!m_user.isEmpty())
	{
		s << "-u" << m_user;
	}

	if (m_status != P4RequestChanges::NONE)
	{
		QString str(QMetaEnum::fromType<P4RequestChanges::ChangeStatus>().key(m_status));
		s << "-s" << str.toLower();
	}

	s << "-m" << "100";
	s << "-l";
	s << m_searchPath;

	return s;
}
