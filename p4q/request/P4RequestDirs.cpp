/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "p4q/request/P4RequestDirs.h"
#include "p4q/request/P4RequestDirs_p.h"

P4RequestDirs::P4RequestDirs()
	: P4Request(*new P4RequestDirsPrivate(this), nullptr)
{
	Q_D(P4RequestDirs);

	d->setCommand("dirs");
	d->m_searchPath = "//*";
}

P4RequestDirs::P4RequestDirs(QString path, int searchLevel)
	: P4Request(*new P4RequestDirsPrivate(this), nullptr)
{
	Q_D(P4RequestDirs);

	d->setCommand("dirs");
	d->m_searchPath = path;
	for (int i = 0; i < searchLevel; ++i)
	{
		d->m_searchPath.append("/*");
	}
}

void P4RequestDirs::setSearchPath(const QString& searchPath)
{
	Q_D(P4RequestDirs);

	d->m_searchPath = searchPath;
}
