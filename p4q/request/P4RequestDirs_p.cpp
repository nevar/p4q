/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "p4q/request/P4RequestDirs_p.h"
#include "p4q/request/P4Tag.h"

P4RequestDirsPrivate::P4RequestDirsPrivate(P4RequestDirs* q)
	: P4RequestPrivate(q)
{

}

bool P4RequestDirsPrivate::on_newMessage(P4Message* msg)
{
	bool handled = P4RequestPrivate::on_newMessage(msg);
	if (handled)
		return true;

	if (msg->itemHash().value(P4Tag2::k_func) == P4Tag2::v_clientFstatInfo)
	{
		m_resultPaths.append(msg->value(P4Tag2::k_dir));
		return true;
	}

	return false;
}

void P4RequestDirsPrivate::finished()
{
	Q_Q(P4RequestDirs);
	P4RequestPrivate::finished();
	emit q->dirsChanged(m_searchPath, m_resultPaths);
}

void P4RequestDirsPrivate::started()
{
	m_resultPaths.clear();
}

QStringList P4RequestDirsPrivate::getArgs() const
{
	QStringList s;
	s << m_searchPath;

	return s;
}
