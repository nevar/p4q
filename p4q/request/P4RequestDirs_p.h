/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef P4REQUESTDIRS_P_H
#define P4REQUESTDIRS_P_H

#include "p4q/request/P4Request_p.h"
#include "p4q/request/P4RequestDirs.h"
#include <QtCore/QObject>

class P4RequestDirsPrivate : public P4RequestPrivate
{
	Q_OBJECT
	Q_DECLARE_PUBLIC(P4RequestDirs)
	Q_DISABLE_COPY(P4RequestDirsPrivate)

private:
	P4RequestDirsPrivate(P4RequestDirs* q);

	bool on_newMessage(P4Message *msg) final;
	void finished () final;
	void started() final;
	QStringList getArgs() const final;

	QString m_searchPath;
	QStringList m_resultPaths;
};

#endif // P4REQUESTDIRS_P_H