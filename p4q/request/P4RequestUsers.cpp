/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "p4q/request/P4RequestUsers.h"
#include "p4q/request/P4RequestUsers_p.h"

P4RequestUsers::P4RequestUsers(QObject *parent)
	: P4Request(*new P4RequestUsersPrivate(this), parent)
{
	Q_D(P4RequestUsers);
	setObjectName("requestUsers");
	d->m_flags = P4_REQUEST_NONE;
	d->setCommand("users");
}

P4RequestUsers::~P4RequestUsers()
{
}

void P4RequestUsers::setFlags(P4RequestUsersFlags flags)
{
	Q_D(P4RequestUsers);
	d->m_flags = flags;
}

void P4RequestUsers::setFlag(P4RequestUsers::P4RequestUsersFlags flags)
{
	Q_D(P4RequestUsers);
	d->m_flags = (P4RequestUsers::P4RequestUsersFlags)(d->m_flags | flags);
}

void P4RequestUsers::resetFlag(P4RequestUsers::P4RequestUsersFlags flags)
{
	Q_D(P4RequestUsers);
	d->m_flags = (P4RequestUsers::P4RequestUsersFlags)(d->m_flags ^ flags);
}

void P4RequestUsers::setMax(int max)
{
	Q_D(P4RequestUsers);
	d->m_maxUsers = max;
}

void P4RequestUsers::setUser(QString user)
{
	Q_D(P4RequestUsers);
	d->m_user = user;
}

QList<P4User> P4RequestUsers::getUsers() const
{
	Q_D(const P4RequestUsers);
	return d->m_users;
}
