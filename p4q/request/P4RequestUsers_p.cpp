/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "p4q/request/P4RequestUsers_p.h"
#include "p4q/request/P4RequestUsers.h"
#include "p4q/request/P4Tag.h"

P4RequestUsersPrivate::P4RequestUsersPrivate(P4RequestUsers* q)
	: P4RequestPrivate(q)
	, m_maxUsers(-1)
{
}

QStringList P4RequestUsersPrivate::getArgs() const
{
	QStringList args;

	if (m_flags & P4RequestUsers::P4_REQUEST_ALL)
		args << "-a";
	if (m_flags & P4RequestUsers::P4_REQUEST_LOGIN_INFO)
		args << "-l";
	if (m_flags & P4RequestUsers::P4_REQUEST_REPLICA_MASTER)
		args << "-c";
	if (m_flags & P4RequestUsers::P4_REQUEST_REPLICA_CURRENT)
		args << "-r";
	if (m_maxUsers >= 0)
		args << "-m" << QString("%1").arg(m_maxUsers);
	if (!m_user.isEmpty())
		args << m_user;

	return args;
}

bool P4RequestUsersPrivate::on_newMessage(P4Message* msg)
{
	bool handled = P4RequestPrivate::on_newMessage(msg);
	if (handled)
		return true;

	if (msg->itemHash().value(P4Tag2::k_func) == P4Tag2::v_clientFstatInfo)
	{
		QHash<QString, QString> h(msg->itemHash());
		h.remove(P4Tag2::k_func);
		P4User user(h);
		m_users.append(user);
		return true;
	}

	return false;
}

void P4RequestUsersPrivate::finished()
{
	Q_Q(P4RequestUsers);
	P4RequestPrivate::finished();
	emit q->usersChanged(m_users);
}

void P4RequestUsersPrivate::started()
{
	m_users.clear();
	P4RequestPrivate::started();
}
