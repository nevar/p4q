/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef P4REQUESTWORKSPACES_H
#define P4REQUESTWORKSPACES_H

#include <p4q/request/P4Request.h>
#include <p4q/data/P4Workspace.h>
#include <p4q/P4Global.h>

class P4RequestWorkspacesPrivate;

class P4QSHARED_EXPORT P4RequestWorkspaces : public P4Request
{
	Q_OBJECT
	Q_DECLARE_PRIVATE(P4RequestWorkspaces)
	Q_DISABLE_COPY(P4RequestWorkspaces)

public:
	P4RequestWorkspaces(QObject *parent = nullptr);

signals:
	void workspacesChanged(const QList<P4Workspace> workspaces);
};

#endif // P4REQUESTWORKSPACES_H