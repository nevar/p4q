#include "p4q/request/P4RequestWorkspaces_p.h"
#include "p4q/request/P4Tag.h"

P4RequestWorkspacesPrivate::P4RequestWorkspacesPrivate(P4RequestWorkspaces* q)
	: P4RequestPrivate(q)
{

}

QStringList P4RequestWorkspacesPrivate::getArgs() const
{
	QStringList s;
	return s;
}

bool P4RequestWorkspacesPrivate::on_newMessage(P4Message* msg)
{
	bool handled = P4RequestPrivate::on_newMessage(msg);
	if (handled)
		return true;

	if (msg->itemHash().value(P4Tag2::k_func) == P4Tag2::v_clientFstatInfo)
	{
		QHash<QString, QString> h(msg->itemHash());
		h.remove(P4Tag2::k_func);
		P4Workspace user(h);
		m_workspaces.append(user);
		return true;
	}

	return false;
}

void P4RequestWorkspacesPrivate::finished()
{
	Q_Q(P4RequestWorkspaces);
	P4RequestPrivate::finished();
	emit q->workspacesChanged(m_workspaces);
}

void P4RequestWorkspacesPrivate::started()
{
	m_workspaces.clear();
	P4RequestPrivate::started();
}
