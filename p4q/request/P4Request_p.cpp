/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "p4q/request/P4Request_p.h"
#include "p4q/request/P4Request.h"
#include "p4q/request/P4Tag.h"
#include "p4q/P4Client.h"
#include "p4q/Log.h"
#include "p4q/data/P4Result_p.h"
#include "p4q/data/P4User.h"
#include "p4q/data/P4Change.h"
#include "p4q/data/P4Workspace.h"
#include "p4q/data/P4Error.h"

P4RequestPrivate::P4RequestPrivate(P4Request* q)
	: q_ptr(q)
{
}

P4RequestPrivate::~P4RequestPrivate()
{
	disconnect();
}

bool P4RequestPrivate::on_newMessage(P4Message* msg)
{
	Q_Q(P4Request);

	// TODO change this if else to Map<string,func>
	if (msg->value(P4Tag2::k_func) == P4Tag2::v_clientMessage)
	{
		msg->removeItem(P4Tag2::k_func);
		bool errorAdded = false;
		QString k_code_base = P4Tag2::k_code;
		QString k_fmt_base = P4Tag2::k_fmt;
		k_code_base.chop(1);
		k_fmt_base.chop(1);

		// Used 16 here to avoid endless while if someone will try to DDOS from
		// protocol level. I have only seen 2 values in protocol implementation
		for (int i = 0; i < 16; ++i)
		{
			QString k_code = QString("%1%2").arg(k_code_base).arg(i);
			QString k_fmt = QString("%1%2").arg(k_fmt_base).arg(i);

			if (msg->containsKey(k_code) && P4Error::severity(msg->value(k_code)) != P4ERROR_INFO )
			{
				QString formatString = msg->value(k_fmt);
				QStringList formatStringList = formatString.split("%");
				formatString.clear();

				int j = 0;
				foreach (QString arg, formatStringList)
				{
					if (j & 1)
					{
						if (msg->containsKey(arg))
						{
							formatString.append(msg->value(arg));
						}
						else
						{
							formatString.append(QString("\%1\%").arg(arg));
						}
					}
					else
					{
						formatString.append(arg);
					}
					++j;
				}

				QString inf(QString("[%1] %2")
							.arg(msg->value(k_code))
							.arg(formatString));
				m_result.appendResult(inf, P4Error::resultType(msg->value(k_code)));
				errorAdded = true;
			}
			else
			{
				msg->removeItem(k_code);
				msg->removeItem(k_fmt);
			}
		}

		if (errorAdded)
			return true;
	}

	return false;
}

void P4RequestPrivate::registerTypes()
{
	static bool registered = false;

	if (!registered)
	{
		qRegisterMetaType<P4Request*>("P4Request*");
		qRegisterMetaType<QSharedPointer<P4Request>>("QSharedPointer<P4Request>");
		qRegisterMetaType<P4Result>("P4Result");
		qRegisterMetaType<QList<P4User>>("QList<P4User>");
		qRegisterMetaType<QList<P4Change>>("QList<P4Change>");
		qRegisterMetaType<QList<P4Workspace>>("QList<P4Workspace>");

		registered = true;
	}
}

QStringList P4RequestPrivate::getArgs() const
{
	return QStringList();
}

void P4RequestPrivate::started()
{
	Q_Q(P4Request);
	m_timer.start();
	emit q->started(q->fullCommand());
}

void P4RequestPrivate::finished()
{
	Q_Q(P4Request);

	int time = m_timer.elapsed();
	emit q->finished(q->fullCommand(), m_result, time);
}

void P4RequestPrivate::setCommand(const QString &cmd)
{
	m_command = cmd;
}

void P4RequestPrivate::setUser(const QString& aUser)
{
	m_user = aUser;
}

QString P4RequestPrivate::user() const
{
	return m_user;
}

void P4RequestPrivate::setWorkspace(const QString& aWorkspace)
{
	m_workspace = aWorkspace;
}

QString P4RequestPrivate::workspace() const
{
	return m_workspace;
}

P4Message P4RequestPrivate::newMsg(const P4Connection &protocol) const
{
	QString func(QString("user-%1").arg(m_command));
	P4Message msg;

	// FIXME add arguments
	foreach (QString arg, getArgs())
	{
		msg.appendItem(P4Tag2::k_empty, arg);
	}

	// FIXME fill proper values
//	msg.insert(P4Tag2::k_autologin, "");
//	msg.insert(P4Tag2::k_version, "2016.1/FREEBSD100X86_64/1492381");
//	msg.insert(P4Tag2::k_prog, "p4");
//	msg.insert(P4Tag2::k_host, "host");
//	msg.insert(P4Tag2::k_clientcase, "0");
//	msg.insert("progress", "1");
	msg.appendItem(P4Tag2::k_client, "");
	msg.appendItem(P4Tag2::k_cwd, "");
	msg.appendItem(P4Tag2::k_os, "UNIX");
	msg.appendItem(P4Tag2::k_user, user());
	msg.appendItem(P4Tag2::k_charset, QString("%1").arg(P4Tag2::UTF_8));
	msg.appendItem(P4Tag2::k_func, func);

	return msg;
}

void P4RequestPrivate::appendResult(QString msg, P4ResultType type)
{
	m_result.appendResult(msg, type);
}
