/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef P4REQUESTPRIVATE_P_H
#define P4REQUESTPRIVATE_P_H

#include "p4q/P4Transport.h"
#include "p4q/data/P4Result.h"
#include "p4q/P4Connection.h"
#include <QtCore/QString>
#include <QtCore/QTime>
#include <QtCore/QHash>

class P4Request;
class P4Client;

class P4RequestPrivate : public QObject
{
	Q_OBJECT
	Q_DECLARE_PUBLIC(P4Request)

protected:
	P4RequestPrivate(P4Request *q);
	~P4RequestPrivate();

	void registerTypes();	// register all types needed by all requests

public:
	// can be implemented by derived classes
	virtual QStringList getArgs() const;
	virtual bool on_newMessage(P4Message *msg);
	virtual void started();
	virtual void finished();

protected:
	void setCommand(const QString& cmd);

public:
	void setUser(const QString& aUser);
	QString user() const;
	void setWorkspace(const QString& aWorkspace);
	QString workspace() const;
	virtual P4Message newMsg(const P4Connection &protocol) const;
	void appendResult(QString msg, P4ResultType type);

protected:
	P4Request *q_ptr;

private:
	QString m_command;
	P4Result m_result;
	QTime m_timer;
	P4Transport *m_transport;
	QString m_user;
	QString m_workspace;
};

#endif // P4REQUESTPRIVATE_P_H
