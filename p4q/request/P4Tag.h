#ifndef P4TAG_H
#define P4TAG_H

namespace P4Tag2
{
	/*
	 * k_xxx key strings
	 * v_xxx value strings
	 */

enum P4Charset {
	NO_CHARSET = 0, UTF_8, ISO8859_1, UTF_16, SHIFTJIS, EUCJP,
	WIN_US_ANSI, WIN_US_OEM, MACOS_ROMAN, ISO8859_15, ISO8859_5,
	KOI8_R, WIN_CP_1251, UTF_16_LE, UTF_16_BE,
	UTF_16_LE_BOM, UTF_16_BE_BOM, UTF_16_BOM, UTF_8_BOM, UTF_32,
	UTF_32_LE, UTF_32_BE, UTF_32_LE_BOM, UTF_32_BE_BOM, UTF_32_BOM,
	UTF_8_UNCHECKED, UTF_8_UNCHECKED_BOM, CP949, CP936, CP950,
	CP850, CP858, CP1253, CP737, ISO8859_7, CP1250, CP852, ISO8859_2
};

	static constexpr const char* k_empty = "";				// used to append args

	static constexpr const char* k_api = "api";				// NOTE what it is?
	static constexpr const char* k_autologin = "autoLogin";	// NOTE what it is?
	static constexpr const char* k_broker = "broker";		// NOTE what it is?
	static constexpr const char* k_charset = "charset";		// charset type see P4Tag::P4Charset enum
	static constexpr const char* k_client = "client";		// client version / or workspace name
	static constexpr const char* k_clientcase = "clientCase";	// NOTE what it is? client version? / or workspace name
	static constexpr const char* k_cmpfile = "cmpfile";		// NOTE what it is?
	static constexpr const char* k_code = "code0";			// status return value
	static constexpr const char* k_cwd = "cwd";				// working directory
	static constexpr const char* k_daddr = "daddr";			// destination address
	static constexpr const char* k_dir = "dir";				// path from dirs command
	static constexpr const char* k_enablestreams = "enableStreams";				// NOTE what it is?
	static constexpr const char* k_expandandmaps = "expandAndmaps";				// NOTE what it is?
	static constexpr const char* k_fmt = "fmt0";			// return value format string
	static constexpr const char* k_func = "func";			// message type
	static constexpr const char* k_fseq = "fseq";			// used with flush messages
	static constexpr const char* k_himark = "himark";		// used with flush messages
	static constexpr const char* k_host = "host";			// host name
	static constexpr const char* k_os = "os";				// OS type UNIX
	static constexpr const char* k_port = "port";			// port
	static constexpr const char* k_prog = "prog";			// application name
	static constexpr const char* k_proxy = "proxy";			// NOTE what it is?
	static constexpr const char* k_rcvbuf = "rcvbuf";		// NOTE what it is?
	static constexpr const char* k_revver = "revver";		// NOTE what it is?
	static constexpr const char* k_security = "security";	// NOTE what it is?
	static constexpr const char* k_server = "server";		// NOTE what it is?
	static constexpr const char* k_server2 = "server2";		// NOTE what it is?
	static constexpr const char* k_serverAddress = "serverAddress";		// server address used to get ticket
	static constexpr const char* k_serverid = "serverID";	// NOTE what it is?
	static constexpr const char* k_sndbuf = "sndbuf";		// NOTE what it is?
	static constexpr const char* k_token = "token";			// auth token
	static constexpr const char* k_tzoffset = "tzoffset";	// NOTE what it is?
	static constexpr const char* k_user = "user";			// user name
	static constexpr const char* k_version = "version";		// OS version like UA "2016.1/FREEBSD100X86_64/1492381"
	static constexpr const char* k_xfiles = "xfiles";		// NOTE what it is?

	// user info
	static constexpr const char* k_user_access = "Access";		// "access" date in date format
	static constexpr const char* k_user_authMethod = "AuthMethod";	// authentication method (perforce,ldap)
	static constexpr const char* k_user_email = "Email";		// email address
	static constexpr const char* k_user_fullName = "FullName";	// full user name
	static constexpr const char* k_user_jobView = "JobView";	// user job view filter
	static constexpr const char* k_user_reviews = "Reviews";	// user reviews
	static constexpr const char* k_user_type = "Type";			// user type
	static constexpr const char* k_user_name = "User";			// user name
	static constexpr const char* k_user_update = "Update";		// "update" date in date format
	static constexpr const char* k_user_password = "Password";	// is password enabled

	// workspace info
	static constexpr const char* k_workspace_access = "Access";			// last access date
	static constexpr const char* k_workspace_altRoots = "AltRoots0";	// alternative root paths
	static constexpr const char* k_workspace_desc = "Description";		// description
	static constexpr const char* k_workspace_host = "Host";				// hostname
	static constexpr const char* k_workspace_lineEnd = "LineEnd";		// line ending type (unix/local/share)
	static constexpr const char* k_workspace_name = "client";			// workspace name
	static constexpr const char* k_workspace_options = "Options";		// options (allwrite/noclobber/nocompress/unlocked/nomodtime/rmdir)
	static constexpr const char* k_workspace_owner = "Owner";			// owner
	static constexpr const char* k_workspace_root = "Root";				// root mount path
	static constexpr const char* k_workspace_submitOptions = "SubmitOptions";	// submit options (submitunchanged)
	static constexpr const char* k_workspace_type = "Type";				// type (writable)
	static constexpr const char* k_workspace_update = "Update";			// last update time
	static constexpr const char* k_workspace_view = "View";				// view, mapping of depot paths

	// changes info
	static constexpr const char* k_change_id = "change";				// change id of a change
	static constexpr const char* k_change_type = "changeType";			// type of a change
	static constexpr const char* k_change_client = "client";			// workspace name of a change
	static constexpr const char* k_change_desc = "desc";				// commit message of a change
	static constexpr const char* k_change_owner = "user";				// owner of a change
	static constexpr const char* k_change_path = "path";				// base path of a change
	static constexpr const char* k_change_status = "status";			// current status of a change
	static constexpr const char* k_change_time = "time";				// update time of a change

	// func user values
	static constexpr const char* u_users	= "user-users";	// Users info

	// func values
	static constexpr const char* v_clientFstatInfo	= "client-FstatInfo";	// User info
	static constexpr const char* v_clientCrypto		= "client-Crypto";		// handle auth/pass
	static constexpr const char* v_clientMessage	= "client-Message";		// std message

	static constexpr const char* v_crypto			= "crypto";			// token response to client-Crypto
	static constexpr const char* v_flush1			= "flush1";			// protocol mark from server
	static constexpr const char* v_flush2			= "flush2";			// response to flush1
	static constexpr const char* v_protocol			= "protocol";		// one of init messages
	static constexpr const char* v_release			= "release";		// end of transmission

}

#endif // P4TAG_H
