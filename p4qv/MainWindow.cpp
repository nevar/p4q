/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <p4q/request/P4RequestUsers.h>
#include <request/P4RequestChanges.h>
#include <QtCore/QDebug>

MainWindow::MainWindow(QString host, qint16 port, QString user, QString pass, QString workspace, QWidget *parent)
	: QMainWindow(parent)
	, ui(new Ui::MainWindow)
	, m_p4Client(this)
	, m_user(user)
	, m_password(pass)
	, m_workspace(workspace)
    , m_depotModel(m_p4Client, m_user)
	, m_requestsWaiting(0)
	, m_idleConnetions(0)
	, m_busyConnections(0)
	, m_progressBar(new QProgressBar())
	, m_progressLabel(new QLabel("0"))
{
	m_p4Client.setConnectionInfo(host, port);
	ui->setupUi(this);

	// ### USERS TAB ###
	m_usersFilter.setSourceModel(&m_usersModel);
//	ui->usersView->setModel(&m_usersFilter);
	ui->usersTypeFilter->addItem("User id");
	ui->usersTypeFilter->addItem("Name");
	ui->usersTypeFilter->addItem("Type");
	ui->usersTypeFilter->addItem("Access date");
	ui->usersTypeFilter->addItem("Update date");
	ui->usersTypeFilter->addItem("Email");
	ui->usersTypeFilter->addItem("Password enabled");
	// show user details on selection changed
	QObject::connect(ui->usersView->selectionModel(), &QItemSelectionModel::currentChanged,
					 this, &MainWindow::on_usersView_currentChanged);
	// update users count on rows changed
	QObject::connect(&m_usersModel, &UsersModel::updated,
					 this, &MainWindow::on_usersModel_rowsChanged);

	// ### CHANGES TAB ###
	ui->depotView->setModel(&m_depotModel);
	ui->changesView->setModel(&m_changesModel);
	m_usersModelEmptyVal.refresh(&m_p4Client, m_user);
	m_usersTest.refresh(&m_p4Client, m_user);
//	ui->changesUserFilter->setModel(&m_usersModelEmptyVal);
//	ui->changesUserFilter->setModel(&m_usersModel);
	ui->usersView->setModel(&m_usersTest);
	QObject::connect(ui->depotView->selectionModel(), &QItemSelectionModel::currentChanged,
					 this, &MainWindow::on_depotSelectionChanged);

	// ### WORKSPACES TAB ###
	m_workspacesFilter.setSourceModel(&m_workspacesModel);
	ui->workspacesUserFilter->setModel(&m_usersModel);
	ui->workspacesView->setModel(&m_workspacesFilter);
	QObject::connect(ui->workspacesView->selectionModel(), &QItemSelectionModel::selectionChanged,
					 this, &MainWindow::on_wokrspacesView_currentChanged);
	QObject::connect(&m_workspacesModel, &WorkspacesModel::updated,
					 this, &MainWindow::on_workspacesModel_rowsChanged);

	// connect P4Client to the logger
	QObject::connect(&m_p4Client, &P4Client::stateChanged, this, &MainWindow::on_currentRequestsChanged);
	QObject::connect(&m_p4Client, &P4Client::requestSent, this, &MainWindow::on_client_started);
	QObject::connect(&m_p4Client, &P4Client::requestFinished, this, &MainWindow::on_client_finished);
	QObject::connect(&m_p4Client, &P4Client::requestError, this, &MainWindow::on_client_error);
	statusBar()->addPermanentWidget(m_progressLabel);
	statusBar()->addPermanentWidget(m_progressBar);
	m_progressBar->setMaximumWidth(100);

	on_actionRefresh_triggered();
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::on_client_error(P4Result res)
{
	updateStatusBar(res.toString());
}

void MainWindow::on_client_started(const QString cmd)
{
	updateStatusBar(cmd);
}

void MainWindow::on_client_finished(const P4Request *req, const P4Result res, int msDuration)
{
	if (!res.isEmpty())
	{
		updateStatusBar(res.toString());
	}
}

void MainWindow::on_client_connected()
{
	ui->p4connectButton->setText("Disconnect");
}

void MainWindow::on_client_disconnected()
{
	ui->p4connectButton->setText("Connect");
}

void MainWindow::on_p4connectButton_clicked()
{
	if (m_p4Client.isConnected())
		m_p4Client.disconnect();
	else
		m_p4Client.connect();
}

void MainWindow::updateStatusBar(QString msg)
{
	QString str(QString("p4 %1").arg(msg));
	log(str);
	ui->statusBar->showMessage(str, s_messageTimeoutms);
}

void MainWindow::log(QString str)
{
	int index = ui->tabWidget->indexOf(ui->logBox->parentWidget());

	ui->logBox->appendPlainText(str);
	if (ui->tabWidget->currentIndex() != index)
		ui->tabWidget->setTabText(index, "Log*");
}

void MainWindow::on_usersView_currentChanged(const QModelIndex& current, const QModelIndex& previous)
{
	ui->userName->setText(m_usersFilter.data(current, UsersModel::ROLE_ID).toString());
	ui->userFullName->setText(m_usersFilter.data(current, UsersModel::ROLE_FULL_NAME).toString());
	ui->userType->setText(m_usersFilter.data(current, UsersModel::ROLE_TYPE).toString());
	ui->userAccessDate->setText(m_usersFilter.data(current, UsersModel::ROLE_ACCESS_DATE).toString());
	ui->userUpdateDate->setText(m_usersFilter.data(current, UsersModel::ROLE_UPDATE_DATE).toString());
	ui->userEmail->setText(m_usersFilter.data(current, UsersModel::ROLE_EMAIL).toString());
	ui->userPassword->setText(m_usersFilter.data(current, UsersModel::ROLE_PASSWORD_ENABLED).toString());
	ui->userJobs->setText(m_usersFilter.data(current, UsersModel::ROLE_JOB_VIEW).toString());
	ui->userReviews->setText(m_usersFilter.data(current, UsersModel::ROLE_REVIEWS).toString());
}

void MainWindow::on_wokrspacesView_currentChanged(const QItemSelection &selected, const QItemSelection &deselected)
{
	if (selected.isEmpty())
		return;

	ui->workspaceName->setText(m_workspacesFilter.data(selected.indexes().at(0), WorkspacesModel::ROLE_ID).toString());
	ui->workspaceDescription->setText(m_workspacesFilter.data(selected.indexes().at(0), WorkspacesModel::ROLE_DESCRIPTION).toString());
	ui->workspaceAccess->setText(m_workspacesFilter.data(selected.indexes().at(0), WorkspacesModel::ROLE_ACCESS_DATE).toString());
	ui->workspaceUpdate->setText(m_workspacesFilter.data(selected.indexes().at(0), WorkspacesModel::ROLE_UPDATE_DATE).toString());
	ui->workspaceHost->setText(m_workspacesFilter.data(selected.indexes().at(0), WorkspacesModel::ROLE_HOST).toString());
	ui->workspaceType->setText(m_workspacesFilter.data(selected.indexes().at(0), WorkspacesModel::ROLE_TYPE).toString());
	ui->workspaceOptions->setText(m_workspacesFilter.data(selected.indexes().at(0), WorkspacesModel::ROLE_OPTIONS).toString());
	ui->workspaceOwner->setText(m_workspacesFilter.data(selected.indexes().at(0), WorkspacesModel::ROLE_OWNER).toString());
	ui->workspaceRoot->setText(m_workspacesFilter.data(selected.indexes().at(0), WorkspacesModel::ROLE_ROOT).toString());
	ui->workspaceAltRoot->setText(m_workspacesFilter.data(selected.indexes().at(0), WorkspacesModel::ROLE_ALT_ROOTS).toStringList().join("\n"));
	ui->workspaceSubmitOptions->setText(m_workspacesFilter.data(selected.indexes().at(0), WorkspacesModel::ROLE_SUBMIT_OPTIONS).toString());
	ui->workspaceViewMap->setText(m_workspacesFilter.data(selected.indexes().at(0), WorkspacesModel::ROLE_VIEW).toString());
	ui->workspaceLineEnd->setText(m_workspacesFilter.data(selected.indexes().at(0), WorkspacesModel::ROLE_LINE_END).toString());
}

void MainWindow::on_actionRefresh_triggered()
{
	m_workspacesModel.refresh(&m_p4Client, m_user);
//	m_usersModel.refresh(&m_p4Client, m_user);
}

void MainWindow::on_usersValueFilter_textChanged(const QString &arg1)
{
	m_usersFilter.setFilterRegExp(arg1);

	if (m_usersFilter.rowCount() > 0)
	{
		ui->usersView->setCurrentIndex(m_usersFilter.index(0,0));
	}
	on_usersModel_rowsChanged();
}

void MainWindow::on_usersTypeFilterd_currentIndexChanged(int index)
{
	m_usersFilter.setFilterRole(ui->usersTypeFilter->currentIndex() + Qt::UserRole);
	on_usersModel_rowsChanged();
}

void MainWindow::on_usersModel_rowsChanged()
{
	ui->label_usersCount->setText(QString("#%1").arg(m_usersFilter.rowCount()));
}

void MainWindow::on_workspacesModel_rowsChanged()
{
	ui->label_workspacesCount->setText(QString("#%1").arg(ui->workspacesView->model()->rowCount()));
}

void MainWindow::on_workspacesUserFilter_currentIndexChanged(const QString &arg1)
{
	m_workspacesFilter.setFilterRole(WorkspacesModel::ROLE_OWNER);
	m_workspacesFilter.setFilterFixedString(arg1);
	ui->label_workspacesCount->setText(QString("#%1").arg(ui->workspacesView->model()->rowCount()));
}

void MainWindow::on_tabWidget_currentChanged(int index)
{
	QString label = ui->tabWidget->tabText(index);
	if (label.endsWith('*'))
	{
		label.chop(1);
		ui->tabWidget->setTabText(index, label);
	}
}

void MainWindow::on_depotSelectionChanged(const QModelIndex& current, const QModelIndex& previous)
{
	updateChangesModel(current);
}

void MainWindow::on_changesUserFilter_currentIndexChanged(const QString &arg1)
{
	if (ui->depotView->selectionModel())
	{
		QModelIndex index = ui->depotView->selectionModel()->currentIndex();
		updateChangesModel(index);
	}
}

void MainWindow::updateChangesModel(const QModelIndex& index)
{
	if (!index.isValid())
		return;

	DepotItem *item = (DepotItem*)index.internalPointer();
	QString user(ui->changesUserFilter->currentText());

	P4RequestChanges *req = new P4RequestChanges(item->path() + "/...", user);
	req->setStatusFilter((P4RequestChanges::ChangeStatus)ui->changesStatusFilter->currentIndex());

	QObject::connect(req, &P4RequestChanges::changes,
					 &m_changesModel, &ChangesModel::on_newChanges);

	m_p4Client.send(req, m_user);
}

void MainWindow::on_changesStatusFilter_currentIndexChanged(int index)
{
	if (ui->depotView->selectionModel())
	{
		QModelIndex index = ui->depotView->selectionModel()->currentIndex();
		updateChangesModel(index);
	}
}

void MainWindow::on_currentRequestsChanged(int requestsWaiting, int idleConnetions, int busyConnections)
{
	m_progressBar->setRange(0, busyConnections+idleConnetions);
	m_progressBar->setValue(busyConnections);
	m_progressLabel->setText(QString("%1").arg(requestsWaiting+busyConnections));
	QString toolTipMsg(QString("threads: %1, busy: %2")
					.arg(idleConnetions+busyConnections)
					.arg(busyConnections));
	m_progressBar->setToolTip(toolTipMsg);
}
