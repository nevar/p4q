/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "model/UsersModel.h"
#include "model/WorkspacesModel.h"
#include "model/DepotContentModel.h"
#include "model/ChangesModel.h"
#include "model/EmptyValueModel.h"

#include <p4q/P4Client.h>
#include <QtWidgets/QMainWindow>
#include <QtCore/QSortFilterProxyModel>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QLabel>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QString host, qint16 port,
						QString user, QString pass,
						QString workspace,
						QWidget *parent = 0);
	~MainWindow();

private slots:
	void on_client_error(P4Result res);
	void on_client_started(const QString cmd);
	void on_client_finished(const P4Request* req, const P4Result res, int msDuration);
	void on_client_connected();
	void on_client_disconnected();

	void on_p4connectButton_clicked();
	void on_usersView_currentChanged(const QModelIndex &current, const QModelIndex &previous);
	void on_wokrspacesView_currentChanged(const QItemSelection &selected, const QItemSelection &deselected);

	void on_actionRefresh_triggered();
	void on_usersValueFilter_textChanged(const QString &arg1);
	void on_usersTypeFilterd_currentIndexChanged(int index);
	void on_usersModel_rowsChanged();
	void on_workspacesModel_rowsChanged();
	void on_workspacesUserFilter_currentIndexChanged(const QString &arg1);

	void on_tabWidget_currentChanged(int index);
	void on_depotSelectionChanged(const QModelIndex& current, const QModelIndex& previous);

	void on_changesUserFilter_currentIndexChanged(const QString &arg1);

	void on_changesStatusFilter_currentIndexChanged(int index);
	void on_currentRequestsChanged(int requestsWaiting, int idleConnetions, int busyConnections);

private:
	void updateStatusBar(QString msg);
	void log(QString str);
	void updateChangesModel(const QModelIndex& index);

	static const int s_messageTimeoutms = 4000;

private:
	Ui::MainWindow *ui;
	P4Client m_p4Client;
	QString m_user;
	QString m_password;
	QString m_workspace;

	UsersModel m_usersModel;
	EmptyValueModel<UsersModel> m_usersModelEmptyVal;
	TestModel m_usersTest;
	QSortFilterProxyModel m_usersFilter;
	WorkspacesModel m_workspacesModel;
	QSortFilterProxyModel m_workspacesFilter;
	ChangesModel m_changesModel;
	DepotContentModel m_depotModel;

	int m_requestsWaiting;
	int m_idleConnetions;
	int m_busyConnections;

	QProgressBar* m_progressBar;
	QLabel* m_progressLabel;
};

#endif // MAINWINDOW_H
