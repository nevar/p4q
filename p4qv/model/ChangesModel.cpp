/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "ChangesModel.h"

ChangesModel::ChangesModel()
{

}

int ChangesModel::rowCount(const QModelIndex& parent) const
{
	return m_changes.count();
}

int ChangesModel::columnCount(const QModelIndex& parent) const
{
	Q_UNUSED(parent)
	return m_columnCount;
}

QVariant ChangesModel::data(const QModelIndex& index, int role) const
{
	if (index.isValid() && index.row() < m_changes.count())
	{
		const P4Change &u = m_changes.at(index.row());
		if (role == Qt::DisplayRole || role == Qt::EditRole)
		{
			if (index.column() >= m_columnCount)
				return QVariant();

			switch (index.column()) {
			case 0:
				return QVariant(u.changeId());
			case 1:
				return QVariant(u.updateDate().toString("yyyy-MM-dd hh:mm"));
			case 2:
				return QVariant(u.owner());
			case 3:
				return QVariant(u.description());
			}

			return QVariant();
		}
		else if(role == ROLE_ID)
		{
			return QVariant(u.changeId());
		}
		else if(role == ROLE_OWNER)
		{
			return QVariant(u.owner());
		}
		else if(role == ROLE_DESC)
		{
			return QVariant(u.description());
		}
	}

	return QVariant();
}

Qt::ItemFlags ChangesModel::flags(const QModelIndex& index) const
{
	Q_UNUSED(index)

	return Qt::ItemFlag::ItemIsSelectable |
			Qt::ItemFlag::ItemIsEnabled |
			Qt::ItemFlag::ItemNeverHasChildren;
}

void ChangesModel::on_newChanges(const QString& path, const QString& user, QList<P4Change> changes)
{
	Q_UNUSED(path)
	Q_UNUSED(user)

	beginResetModel();
	m_changes.clear();
	endResetModel();

	beginInsertRows(QModelIndex(), 0, changes.count()-1);
	m_changes = changes;
	endInsertRows();
}
