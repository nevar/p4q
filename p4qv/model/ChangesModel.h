/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef CHANGESMODEL_H
#define CHANGESMODEL_H

#include <QtCore/QAbstractListModel>
#include <data/P4Change.h>

class ChangesModel : public QAbstractListModel
{
	Q_OBJECT

public:
	ChangesModel();

	enum CustomRole
	{
		ROLE_ID = Qt::UserRole,
		ROLE_OWNER,
		ROLE_DESC,
	};

	Q_INVOKABLE virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
	Q_INVOKABLE virtual int columnCount(const QModelIndex &parent = QModelIndex()) const;
	Q_INVOKABLE virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
	Qt::ItemFlags flags(const QModelIndex &index) const;

public slots:
	void on_newChanges(const QString& path, const QString& user, QList<P4Change> changes);

private:
	QList<P4Change> m_changes;
	static const int m_columnCount = 4;
};

#endif // CHANGESMODEL_H