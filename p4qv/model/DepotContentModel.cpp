/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "DepotContentModel.h"
#include "request/P4RequestDirs.h"
#include "Log.h"
#include <algorithm>

DepotContentModel::DepotContentModel(P4Client& client, const QString& user)
	: m_rootItem(new DepotItem(""))
	, m_p4Client(client)
	, m_user(user)
{
	DepotItem *root = new DepotItem("/");
	m_rootItem->append(root);
	m_rootItem->setStatePopulated();
}

QModelIndex DepotContentModel::parent(const QModelIndex& child) const
{
	if (!child.isValid())
		return QModelIndex();

	DepotItem *childItem = indexToItem(child);
	DepotItem *parentItem = childItem->parent();

	if (parentItem == m_rootItem || childItem == m_rootItem)
		return QModelIndex();

	QModelIndex ind = createIndex(parentItem->row(), 0, parentItem);
	return ind;
}

QModelIndex DepotContentModel::index(int row,
									 int column,
									 const QModelIndex& parent) const
{
	if (!hasIndex(row, column, parent))
		return QModelIndex();

	DepotItem *parentItem = indexToItem(parent);
	DepotItem *childItem = parentItem->child(row);

	if (childItem)
	{
		return createIndex(row, column, childItem);
	}

	return QModelIndex();
}

QModelIndex DepotContentModel::index(DepotItem* item) const
{
	if (item != m_rootItem)
	{
		return index(item->row(), 0, index(item->parent()));
	}

	return QModelIndex();
}

int DepotContentModel::rowCount(const QModelIndex& parent) const
{
	DepotItem *parentItem = indexToItem(parent);
	if (parentItem)
		return parentItem->childCount();

	return 0;
}

int DepotContentModel::columnCount(const QModelIndex& parent) const
{
	Q_UNUSED(parent)
	return 1;
}

QVariant DepotContentModel::data(const QModelIndex& index, int role) const
{
	if (!index.isValid())
		return QVariant();

	if (role != Qt::DisplayRole)
		return QVariant();

	DepotItem *item = indexToItem(index);
	return item->valueString();
}

void DepotContentModel::getMore(DepotItem *parentItem) const
{
	if ( parentItem->state() == DepotItem::DEPOT_ITEM_EMPTY ||
		 parentItem->state() == DepotItem::DEPOT_ITEM_POPULATED )
	{
		P4RequestDirs *req = new P4RequestDirs(parentItem->path(), 2);
		QObject::connect(req, &P4RequestDirs::dirsChanged,
						 this, &DepotContentModel::on_dirsChanged);
		m_p4Client.send(req, m_user);
		parentItem->setStateRefreshed();
		QModelIndex ind(index(parentItem));
		emit const_cast<DepotContentModel*>(this)->dataChanged(ind, ind);
	}

	return;
}

bool DepotContentModel::hasChildren(const QModelIndex& parent) const
{
	DepotItem *parentItem = indexToItem(parent);

	if (parentItem == m_rootItem)
		return true;

	getMore(parentItem);

	if (parentItem->childCount() > 0)
	{
		return true;
	}

	return false;
}

DepotItem *DepotContentModel::indexToItem(const QModelIndex &index) const
{
	if (index.isValid())
		return static_cast<DepotItem*>(index.internalPointer());

	return m_rootItem;
}

void DepotContentModel::itemUpdated(DepotItem* item, QList<QString> dirs)
{
}

void DepotContentModel::on_dirsChanged(const QString& requestedPath, const QStringList& returnPaths)
{
	QHash<QString, QString> newItems;

	// collect
	foreach (QString newItem, returnPaths)
	{
		QStringList splitted = newItem.split('/');
		QString value = splitted.last();
		splitted.removeLast();
		newItems.insertMulti(splitted.join('/'), value);
	}

	// process
	QList<QString> keys = newItems.uniqueKeys();
	qSort(keys.begin(), keys.end());
	foreach (QString path, keys)
	{
		QList<QString> items = newItems.values(path);
		std::sort(items.begin(), items.end());

		// find parent item
		DepotItem *parentItem = m_rootItem;
		QModelIndex parentIndex;
		parentItem = m_rootItem->child(0);
		parentIndex = index(0, 0, parentIndex);

		QStringList sp = path.split('/');

		foreach (QString pathItem, sp)
		{
			if (pathItem.isEmpty())
				continue;

			int found = false;
			for (int i = 0; i < parentItem->childCount(); ++i)
			{
				if (parentItem->child(i)->value() == pathItem)
				{
					parentItem = parentItem->child(i);
					parentIndex = index(i, 0, parentIndex);
					found = true;
					break;
				}
			}

			if (!found)
			{
				DepotItem *tmp = new DepotItem(pathItem);
				int tmpi = parentItem->childCount();
				beginInsertRows(parentIndex, tmpi, tmpi);
				parentItem->append(tmp);
				endInsertRows();
				parentItem = tmp;
				parentIndex = index(tmpi, 0, parentIndex);
			}
		}

		// ## add/refresh/remove items ##

		// remove duplicates which are already on the tree
		for (int i = 0; i < parentItem->childCount(); i++)
		{
			items.removeAll(parentItem->child(i)->value());
		}

		beginInsertRows(parentIndex, parentItem->childCount(), parentItem->childCount() + newItems.count() - 1);
		foreach (QString item, items)
		{
			parentItem->append(new DepotItem(item));
		}
		endInsertRows();

		parentItem->setStatePopulated();
		QModelIndex ind(index(parentItem));
		emit dataChanged(ind, ind);
	}
}
