/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef DEPOTCONTENTMODEL_H
#define DEPOTCONTENTMODEL_H

#include "model/DepotItem.h"
#include "request/P4Request.h"
#include <p4q/P4Client.h>
#include <QtCore/QAbstractItemModel>
#include <QtCore/QList>

class DepotItem;

class DepotContentModel : public QAbstractItemModel
{
	Q_OBJECT

public:
	DepotContentModel(P4Client& client, const QString& user);

	Q_INVOKABLE virtual QModelIndex parent(const QModelIndex &child) const final;
	Q_INVOKABLE virtual QModelIndex index(int row, int column,
                              const QModelIndex &parent = QModelIndex()) const final;
	Q_INVOKABLE virtual int rowCount(const QModelIndex &parent = QModelIndex()) const final;
	Q_INVOKABLE virtual int columnCount(const QModelIndex &parent = QModelIndex()) const final;
	Q_INVOKABLE virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const final;
	Q_INVOKABLE virtual bool hasChildren(const QModelIndex &parent = QModelIndex()) const final;

	QModelIndex index(DepotItem* item) const;
	void getMore(DepotItem* parentItem) const;
	DepotItem* createItem(QString name, QList<QString>);

private:
	DepotItem *indexToItem(const QModelIndex& index) const;
	void getParentFromPath(QModelIndex &index, DepotItem *&item, QStringList path);


public slots:
	void itemUpdated(DepotItem* item, QList<QString> dirs);
	void on_dirsChanged(const QString& requestedPath, const QStringList& returnPaths);

private:
	DepotItem *m_rootItem;
	QHash<P4Request*,QModelIndex> m_requestedDirs;
	P4Client &m_p4Client;
	QString m_user;
};

#endif // DEPOTCONTENTMODEL_H
