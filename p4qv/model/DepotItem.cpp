/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "DepotItem.h"
#include "model/DepotContentModel.h"
#include "request/P4RequestDirs.h"
#include "Log.h"

DepotItem::DepotItem(bool isContiainer, QString value, DepotItem* parentItem)
	: m_value(value)
	, m_parent(parentItem)
	, m_isContainer(isContiainer)
	, m_requestSent(false)
	, m_state(DEPOT_ITEM_EMPTY)
{
	if (m_isContainer)
	{
		DepotItem *loadingItem = new DepotItem(false, "updating...", this);
		m_childItems.append(loadingItem);
	}
}

DepotItem::DepotItem(QString value, DepotItem* parentItem)
	: m_value(value)
	, m_parent(parentItem)
	, m_isContainer(true)
	, m_requestSent(false)
	, m_state(DEPOT_ITEM_EMPTY)
{

}

DepotItem::~DepotItem()
{
	clear();
}

int DepotItem::row() const
{
	if (m_parent)
		return m_parent->m_childItems.indexOf(const_cast<DepotItem*>(this));

	return 0;
}

DepotItem* DepotItem::child(int row)
{
	if (row >= 0 && row < m_childItems.count())
		return m_childItems.value(row);

	return nullptr;
}

int DepotItem::childCount() const
{
	return m_childItems.count();
}

DepotItem*DepotItem::parent()
{
	return m_parent;
}

QString DepotItem::value() const
{
	return m_value;
}

QString DepotItem::valueString() const
{
	return m_value;
}

void DepotItem::path(QString& aPath) const
{
	if (m_value.isEmpty())
		return;

	if (m_value == "/")
	{
		aPath = m_value;
		return;
	}
	else
	{
		m_parent->path(aPath);
	}

	aPath += "/" + m_value;
}

QString DepotItem::path() const
{
	QString aPath;
	path(aPath);
	return aPath;
}

void DepotItem::append(DepotItem *item)
{
	item->m_parent = this;
	m_childItems.append(item);
}

DepotItem::DepotItemState DepotItem::state() const
{
	return m_state;
}

void DepotItem::setStatePopulated()
{
	m_state = DEPOT_ITEM_POPULATED;
}

void DepotItem::setStateEmpty()
{
	m_state = DEPOT_ITEM_EMPTY;
}

void DepotItem::setStateRefreshed()
{
	m_state = DEPOT_ITEM_REFRESH_SENT;
}

bool DepotItem::isContainer() const
{
	return m_isContainer;
}

void DepotItem::clear()
{
	foreach (DepotItem* dp, m_childItems)
	{
		delete dp;
	}
	m_childItems.clear();
}
