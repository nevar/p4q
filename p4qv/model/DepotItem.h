/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef DEPOTITEM_H
#define DEPOTITEM_H

#include "model/DepotContentModel.h"
#include <QtCore/QString>
#include <QtCore/QList>

class DepotItem
{
public:
	enum DepotItemState
	{
		DEPOT_ITEM_EMPTY,
		DEPOT_ITEM_REFRESH_SENT,
		DEPOT_ITEM_POPULATED,
	};

	DepotItem(bool isContiainer, QString value, DepotItem *parentItem = 0);
	DepotItem(QString value, DepotItem *parentItem = 0);
	~DepotItem();

	int row() const;
	int childCount() const;
	DepotItem* child(int row);
	DepotItem* parent();

	QString value() const;
	QString valueString() const;
	void path(QString& aPath) const;
	QString path() const;

	bool isContainer() const;

	void append(DepotItem* item);
	void clear();

	void populate();

	DepotItemState state() const;
	void setStatePopulated();
	void setStateEmpty();
	void setStateRefreshed();

private:
	QString m_value;
	DepotItem *m_parent;
	bool m_isContainer;
	QList<DepotItem*> m_childItems;
	DepotItemState m_state;

public:
	bool m_requestSent;
};

#endif // DEPOTITEM_H
