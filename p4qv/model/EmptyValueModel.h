/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef EMPTYVALUEMODEL_H
#define EMPTYVALUEMODEL_H

#include "model/UsersModel.h"
#include <QtCore/QAbstractItemModel>
#include <QtCore/QDebug>

template <typename Base>
class EmptyValueModel : public Base
{
public:
	EmptyValueModel() {
		qDebug() << "con";
	}

	Q_INVOKABLE virtual int rowCount(const QModelIndex &parent = QModelIndex())
	{
		qDebug() << "a" << parent;
		if (Base::rowCount() == 0)
			return 0;

		return Base::rowCount() + 1;
	}

	Q_INVOKABLE virtual QVariant data(const QModelIndex &ind, int role = Qt::DisplayRole)
	{
		qDebug() << "a" << ind;
		if (ind.row() == 0)
		{
			if (!ind.isValid())
				return QVariant();

			if (role != Qt::DisplayRole)
				return QVariant();

			return QVariant("-");
		}
		else
		{
			QModelIndex newInd = Base::index(ind.row()-1, ind.column(), ind.parent());
//			return Base::data(newInd, role);
		}

		return QVariant();
	}

};

class TestModel : public UsersModel
{
public:
	TestModel();

	Q_INVOKABLE virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
	Q_INVOKABLE virtual QVariant data(const QModelIndex &ind, int role = Qt::DisplayRole) const;

};

#endif // EMPTYVALUEMODEL_H