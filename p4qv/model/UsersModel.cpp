/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "UsersModel.h"
#include "Log.h"
#include <p4q/request/P4RequestUsers.h>

UsersModel::UsersModel()
{

}

int UsersModel::rowCount(const QModelIndex& parent) const
{
	Q_UNUSED(parent)

	return m_users.count();
}

int UsersModel::columnCount(const QModelIndex& parent) const
{
	Q_UNUSED(parent)
	return 1;
}

QVariant UsersModel::data(const QModelIndex& index, int role) const
{
	QVariant v;
	if (index.isValid() && index.row() < m_users.count())
	{
		const P4User &u = m_users.at(index.row());
		if (role == Qt::DisplayRole || role == Qt::EditRole)
		{
			return QVariant(u.name());
		}
		else if(role == ROLE_ID)
		{
			return QVariant(u.name());
		}
		else if(role == ROLE_FULL_NAME)
		{
			return QVariant(u.fullName());
		}
		else if(role == ROLE_TYPE)
		{
			return QVariant(u.type());
		}
		else if(role == ROLE_ACCESS_DATE)
		{
			return QVariant(u.accessDate().toString("yyyy-MM-dd hh:mm"));
		}
		else if(role == ROLE_UPDATE_DATE)
		{
			return QVariant(u.updateDate().toString("yyyy-MM-dd hh:mm"));
		}
		else if(role == ROLE_PASSWORD_ENABLED)
		{
			return QVariant(u.isPassword());
		}
		else if(role == ROLE_EMAIL)
		{
			return QVariant(u.email());
		}
		else if(role == ROLE_REVIEWS)
		{
			return QVariant(u.reviews());
		}
		else if(role == ROLE_JOB_VIEW)
		{
			return QVariant(u.jobsView());
		}
	}

	return QVariant();
}

Qt::ItemFlags UsersModel::flags(const QModelIndex& index) const
{
	Q_UNUSED(index)

	return Qt::ItemFlag::ItemIsSelectable |
			Qt::ItemFlag::ItemIsEnabled |
			Qt::ItemFlag::ItemNeverHasChildren;
}

void UsersModel::refresh(P4Client* client, const QString& user)
{
	P4RequestUsers *req = new P4RequestUsers();
	QObject::connect(req, &P4RequestUsers::usersChanged,
					 this, &UsersModel::on_usersChanged);

	client->send(req, user);
}

void UsersModel::on_usersChanged(const QList<P4User> users)
{
	beginResetModel();
	m_users.clear();
	endResetModel();

	beginInsertRows(QModelIndex(), 0, users.count()-1);
	m_users = users;
	endInsertRows();
	emit updated();
}
