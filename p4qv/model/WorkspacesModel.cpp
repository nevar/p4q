/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "WorkspacesModel.h"

WorkspacesModel::WorkspacesModel()
{
}

QVariant WorkspacesModel::data(const QModelIndex& index, int role) const
{
	QVariant v;
	if ( index.isValid() &&
		 (index.row() < m_workspaces.count()))
	{
		const P4Workspace &w = m_workspaces.at(index.row());
		if (role == Qt::DisplayRole || role == Qt::EditRole)
		{
			if (index.column() >= (int)w.hash().count())
				return QVariant();

			switch (index.column()) {
			case 0:
				v = w.owner();
				break;
			case 1:
				v = w.name();
				break;
			}

			return v;
		}
		else if(role == ROLE_ID)
		{
			return QVariant(w.name());
		}
		else if(role == ROLE_DESCRIPTION)
		{
			return QVariant(w.description());
		}
		else if(role == ROLE_ACCESS_DATE)
		{
			return QVariant(w.accessDate().toString("yyyy-MM-dd hh:mm"));
		}
		else if(role == ROLE_UPDATE_DATE)
		{
			return QVariant(w.updateDate().toString("yyyy-MM-dd hh:mm"));
		}
		else if(role == ROLE_HOST)
		{
			return QVariant(w.host());
		}
		else if(role == ROLE_TYPE)
		{
			return QVariant(w.type());
		}
		else if(role == ROLE_OPTIONS)
		{
			return QVariant(w.options());
		}
		else if(role == ROLE_OWNER)
		{
			return QVariant(w.owner());
		}
		else if(role == ROLE_ROOT)
		{
			return QVariant(w.root());
		}
		else if(role == ROLE_ALT_ROOTS)
		{
			return QVariant(w.altRoots());
		}
		else if(role == ROLE_SUBMIT_OPTIONS)
		{
			return QVariant(w.submitOptions());
		}
		else if(role == ROLE_LINE_END)
		{
			return QVariant(w.lineEnd());
		}
		else if(role == ROLE_VIEW)
		{
			return QVariant(w.view());
		}

	}
	return QVariant();
}

QVariant WorkspacesModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role != Qt::DisplayRole)
		return QVariant();

	switch (section) {
	case 0:
		return QVariant("Owner");
		break;
	case 1:
		return QVariant("Name");
		break;
	}

	return QVariant(section);
}

Qt::ItemFlags WorkspacesModel::flags(const QModelIndex& index) const
{
	Q_UNUSED(index)
	return Qt::ItemFlag::ItemIsSelectable |
		   Qt::ItemFlag::ItemIsEnabled |
	        Qt::ItemFlag::ItemNeverHasChildren;
}

void WorkspacesModel::refresh(P4Client* client, const QString& user)
{
	P4RequestWorkspaces *req = new P4RequestWorkspaces();
	QObject::connect(req, &P4RequestWorkspaces::workspacesChanged,
					 this, &WorkspacesModel::on_workspacesChanged);

	client->send(req, user);
}

void WorkspacesModel::on_workspacesChanged(const QList<P4Workspace> workspaces)
{
	beginResetModel();
	m_workspaces.clear();
	endResetModel();

	beginInsertRows(QModelIndex(), 0, workspaces.count()-1);
	m_workspaces = workspaces;
	endInsertRows();

	emit updated();
}

int WorkspacesModel::columnCount(const QModelIndex& parent) const
{
	Q_UNUSED(parent);

	return 2;
}

int WorkspacesModel::rowCount(const QModelIndex& parent) const
{
	Q_UNUSED(parent);
	return m_workspaces.count();
}
