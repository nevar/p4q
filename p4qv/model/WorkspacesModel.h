/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef WORKSPACESMODEL_H
#define WORKSPACESMODEL_H

#include <data/P4Workspace.h>
#include <request/P4RequestWorkspaces.h>
#include <p4q/P4Client.h>
#include <QtCore/QAbstractListModel>

class WorkspacesModel : public QAbstractListModel
{
	Q_OBJECT
public:
	WorkspacesModel();

	enum CustomRole
	{
		ROLE_ID = Qt::UserRole,
		ROLE_DESCRIPTION,
		ROLE_ACCESS_DATE,
		ROLE_UPDATE_DATE,
		ROLE_HOST,
		ROLE_TYPE,
		ROLE_OPTIONS,
		ROLE_OWNER,
		ROLE_ROOT,
		ROLE_ALT_ROOTS,
		ROLE_SUBMIT_OPTIONS,
		ROLE_VIEW,
		ROLE_LINE_END,
	};

	Q_INVOKABLE virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;
	Q_INVOKABLE virtual int columnCount(const QModelIndex &parent = QModelIndex()) const;
	Q_INVOKABLE virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
	Q_INVOKABLE virtual QVariant headerData(int section, Qt::Orientation orientation,
                                int role = Qt::DisplayRole) const;

	Qt::ItemFlags flags(const QModelIndex &index) const;

	void refresh(P4Client *client, const QString& user);

signals:
	void updated();

private slots:
	void on_workspacesChanged(const QList<P4Workspace> workspaces);

private:
	QList<P4Workspace> m_workspaces;
};

#endif // WORKSPACESMODEL_H
