#include "SnoopConnection.h"
#include "p4q/Log.h"

int SnoopConnection::m_connectionsNumber = 0;

SnoopConnection::SnoopConnection(QTcpSocket* socket,
								 QHostAddress serverHost, quint16 serverPort,
								 QHostAddress proxyHost, quint16 proxyPort, QObject *parent)
	: QObject(parent)
{
	QObject::connect(&m_proxyTransport, &P4Transport::disconnectedFromHost,
					 this, &SnoopConnection::on_disconnectedFromClient);
	QObject::connect(&m_proxyTransport, &P4Transport::messagesReadyRead,
					 this, &SnoopConnection::on_clientMessagesReadyRead);

	QObject::connect(&m_serverTransport, &P4Transport::disconnectedFromHost,
					 this, &SnoopConnection::on_disconnectedFromServer);
	QObject::connect(&m_serverTransport, &P4Transport::messagesReadyRead,
					 this, &SnoopConnection::on_serverMessagesReadyRead);

	P4Result res = m_serverTransport.connectToHostSync(serverHost.toString(), serverPort);
	if (res.isError())
	{
		ERR("server transport connection:" << res.toString());
		exit(1);
	}

	m_proxyTransport.setSocket(socket);
	DBG("new connection " << "obj: " << (void*)this << " number: " << m_connectionsNumber << " ip: " << socket->peerAddress().toString() << " port: " << socket->peerPort());


	m_serverTransport.setObjectName(QString("server[%1]").arg(m_connectionsNumber));
	m_serverTransport.showMessagesInLog(true, false);
	m_proxyTransport.setObjectName(QString("proxy[%1]").arg(m_connectionsNumber));
	m_proxyTransport.showMessagesInLog(true, false);
	m_connectionsNumber++;
}

SnoopConnection::~SnoopConnection()
{
	m_connectionsNumber--;
	DBG("del connection " << " obj: " << (void*)this << " number: " << m_connectionsNumber);
}

void SnoopConnection::on_disconnectedFromClient(P4Result res)
{
	deleteLater();
}

void SnoopConnection::on_disconnectedFromServer(P4Result res)
{
	deleteLater();
}

void SnoopConnection::on_clientMessagesReadyRead()
{
	QList<P4Message*> messages(m_proxyTransport.getAllMessages());

	foreach (P4Message* msg, messages)
	{
		m_serverTransport.send(*msg);
		delete msg;
	}
}

void SnoopConnection::on_serverMessagesReadyRead()
{
	QList<P4Message*> messages(m_serverTransport.getAllMessages());

	foreach (P4Message* msg, messages)
	{
		m_proxyTransport.send(*msg);
		delete msg;
	}
}
