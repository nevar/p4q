#ifndef SNOOPCONNECTION_H
#define SNOOPCONNECTION_H

#include "p4q/P4Transport.h"
#include <QtCore/QObject>
#include <QtNetwork/QTcpSocket>
#include <QtNetwork/QHostAddress>

class SnoopConnection : public QObject
{
	Q_OBJECT
public:
	SnoopConnection(QTcpSocket* socket,
					QHostAddress targetHost, quint16 targetPort,
					QHostAddress proxyHost, quint16 proxyPort, QObject *parent = nullptr);

	~SnoopConnection();
signals:

private slots:
	void on_disconnectedFromClient(P4Result res);
	void on_disconnectedFromServer(P4Result res);
	void on_clientMessagesReadyRead();
	void on_serverMessagesReadyRead();

signals:
	void disconnected(SnoopConnection *obj);

private:
	P4Transport m_serverTransport;
	P4Transport m_proxyTransport;

	static int m_connectionsNumber;
};

#endif // SNOOPCONNECTION_H
