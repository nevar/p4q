#include "Snooper.h"
#include "p4q/Log.h"

Snooper::Snooper(QHostAddress servertHost, quint16 serverPort,
				 QHostAddress proxyHost, quint16 proxyPort, QObject *parent)
	: QObject(parent)
	, m_servertHost(servertHost)
	, m_serverPort(serverPort)
	, m_proxyHost(proxyHost)
	, m_proxyPort(proxyPort)
{
	INF("start server on: " << m_proxyHost.toString() << ":" << m_proxyPort);
	if (!m_proxyServer.listen(m_proxyHost, m_proxyPort))
	{
		ERR("Server starting error: " << m_proxyServer.errorString());
		exit(1);
	}

	QObject::connect(&m_proxyServer, &QTcpServer::newConnection,
					 this, &Snooper::on_clientConnected);
}

void Snooper::on_clientConnected()
{
	QTcpSocket *socket = m_proxyServer.nextPendingConnection();
	new SnoopConnection(socket, m_servertHost, m_serverPort, m_proxyHost, m_proxyPort, this);
}
