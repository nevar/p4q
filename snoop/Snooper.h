#ifndef SNOOPER_H
#define SNOOPER_H

#include <QtNetwork/QTcpServer>
#include <QtCore/QObject>
#include "SnoopConnection.h"

class Snooper : public QObject
{
	Q_OBJECT
public:
	Snooper(QHostAddress servertHost, quint16 serverPort,
			QHostAddress proxyHost, quint16 proxyPort, QObject *parent = nullptr);

private slots:
	void on_clientConnected();

private:
	QHostAddress m_servertHost;
	quint16 m_serverPort;
	QHostAddress m_proxyHost;
	quint16 m_proxyPort;
	QTcpServer m_proxyServer;
};

#endif // SNOOPER_H
