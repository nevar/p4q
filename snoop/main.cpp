/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <QtCore/QCoreApplication>
#include <QtCore/QCommandLineParser>
#include "Snooper.h"
#include "p4q/Log.h"

int main(int argc, char *argv[])
{
	QHostAddress serverHost;
	quint16 serverPort = 0;
	QHostAddress proxyHost = QHostAddress::Any;
	quint16 proxyPort = 0;

	QTextStream out(stdout, QIODevice::WriteOnly);
	QCoreApplication a(argc, argv);
	a.setApplicationVersion(VERSION_STRING);

	QCommandLineParser cmdParser;

	QCommandLineOption target(QStringList() << "t" << "target",
							  "Perforce server address", "ip:port");
	QCommandLineOption listen(QStringList() << "l" << "listen",
							  "Address on which new fake server will be started", "ip:port");
	QCommandLineOption debug(QStringList() << "d" << "debug",
							  QString("Debug tags to be printed: %1").arg(Log::logTypes().join(' ')), "tag,tag,...");

	cmdParser.addHelpOption();
	cmdParser.addVersionOption();
	cmdParser.addOption(target);
	cmdParser.addOption(listen);
	cmdParser.addOption(debug);
	cmdParser.process(a);

	QString targetArg(cmdParser.value(target));
	QString listenArg(cmdParser.value(listen));
	QString debugArg(cmdParser.value(debug));
	QStringList targetList(targetArg.split(':'));
	QStringList listenList(listenArg.split(':'));

	Log::m_allowedLogTypes = debugArg.split(',');

	if (targetArg.isEmpty())
	{
		cmdParser.showHelp();
		exit(1);
	}

	if (targetArg.count() > 0)
		serverHost = targetList[0];

	if (targetArg.count() > 1)
	{
		bool isSuccess = false;
		quint16 tmp = targetList[1].toInt(&isSuccess);
		if (isSuccess)
		{
			serverPort = tmp;
			proxyPort = tmp;
		}
	}

	if (listenArg.count() > 0)
		proxyHost = listenList[0];

	if (listenArg.count() > 1)
	{
		bool isSuccess = false;
		quint16 tmp = listenList[1].toInt(&isSuccess);
		if (isSuccess)
			proxyPort = tmp;
	}

	// target host and port are mendatory to start
	if (!serverHost.isNull() && serverPort != 0)
	{
		INF("Forwarding " << serverHost.toString() << ":" << serverPort << " <-> " << proxyHost.toString() << ":" << proxyPort);
		Snooper s(serverHost, serverPort, proxyHost, proxyPort);
		return a.exec();
	}

	out << "Invalid arguments. You need to specify at least perforce server to start" << "\n";
	out.flush();
	cmdParser.showHelp();
}
