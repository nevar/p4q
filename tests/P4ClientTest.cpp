/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "P4ClientTest.h"
#include <QtCore/QSharedPointer>

#define USER "user"
#define HOST "localhost"
#define PORT 1666

#define DB_DIR "db"

P4ClientTest::P4ClientTest()
{
	QObject::connect(&m_p4Client, &P4Client::requestError,
					 this, &P4ClientTest::on_error);

	argv[0] = new char(argc);
	argv[0] = 0;
	m_app = new QCoreApplication(argc, argv);
}

P4ClientTest::~P4ClientTest()
{
	m_p4Server.stop();
}

void P4ClientTest::simpleRequest()
{
	m_p4Server.start(DB_DIR);

//	m_p4Client.setConnectionInfo(HOST, PORT);
//	m_p4Client.setDefaultUser(USER);

//	QSharedPointer<P4RequestUsers> req(new P4RequestUsers());
//	QObject::connect(req.data(), &P4Request::finished,
//					 this, &P4ClientTest::on_finished);

//	m_p4Client.send(req);
//	m_app->exec();

//	QCOMPARE(m_p4Client.host(), QString(HOST));
//	QCOMPARE(m_p4Client.port(), (qint16)PORT);
//	QCOMPARE(m_res.toString(), QString());
//	QCOMPARE(m_res.isError(), false);
	m_p4Server.stop();
}

void P4ClientTest::noServer()
{
//	m_p4Server.stop();
//	m_p4Client.setConnectionInfo(HOST, PORT);

//	QSharedPointer<P4RequestUsers> req(new P4RequestUsers());
//	QObject::connect(req.data(), &P4Request::finished,
//					 this, &P4ClientTest::on_finished);

//	m_p4Client.send(req, USER);
//	m_app->exec();

//	QCOMPARE(m_p4Client.host(), QString(HOST));
//	QCOMPARE(m_p4Client.port(), (qint16)PORT);
//	QCOMPARE(m_res.isError(), true);
}

void P4ClientTest::on_finished(const QString& cmd, const P4Result& res, int msDuration)
{
	m_res = res;
	m_app->quit();
}

void P4ClientTest::on_error(QSharedPointer<P4Request> req)
{
	m_res = req->result();
	m_app->quit();
}
