/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef P4CLIENTTEST_H
#define P4CLIENTTEST_H

#include <p4q/P4Client.h>
#include "P4Server.h"
#include "p4q/request/P4RequestUsers.h"
#include <QtTest/QtTest>

#define USER_NAME "test_user"

class P4ClientTest : public QObject
{
	Q_OBJECT

public:
	P4ClientTest();
	~P4ClientTest();

private slots:
	void simpleRequest();
	void noServer();

public slots:
	void on_finished(const QString& cmd, const P4Result& res, int msDuration);
	void on_error(QSharedPointer<P4Request> req);

private:
	QCoreApplication *m_app;
	P4Client m_p4Client;
	P4Server m_p4Server;
	P4Result m_res;

	int argc{1};
	char *argv[1] = {nullptr};
};

#endif // P4CLIENTTEST_H
