#include "P4ConnectionTest.h"
#include <p4q/P4Connection.h>
#include "P4Server.h"

#define USER "user"
#define HOST "localhost"
#define PORT 1666
#define DB_DIR "db"

void P4ConnectionTest::connect()
{
	P4Server srv;
	srv.start(DB_DIR);

	P4Connection conn;
	QCOMPARE(conn.isConnected(), false);

	P4Result res = conn.connectToHost(HOST, PORT);

	QCOMPARE(res.isError(), false);
	QCOMPARE(conn.isConnected(), true);
	QCOMPARE(conn.host(), HOST);
	QCOMPARE(conn.port(), PORT);

	res = conn.disconnectFromHost();
	QCOMPARE(res.isError(), false);
	qDebug() << res.toString();
	QCOMPARE(conn.isConnected(), false);
	QCOMPARE(conn.host() != HOST, true);
	QCOMPARE(conn.port() != PORT, true);

	srv.stop();
}
