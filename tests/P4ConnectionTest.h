#ifndef P4CONNECTIONTEST_H
#define P4CONNECTIONTEST_H

#include <QtTest/QtTest>

class P4ConnectionTest : public QObject
{
	Q_OBJECT
private slots:
	void connect();

};

#endif // P4CONNECTIONTEST_H