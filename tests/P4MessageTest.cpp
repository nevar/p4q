#include "P4MessageTest.h"
#include <p4q/data/P4Message.h>
#include <QtTest/QTest>
#include <QtCore/QList>
#include <QtCore/QString>

typedef QList<P4Pair> MessageItems;

void P4MessageTest::copy_data()
{
	QTest::addColumn<MessageItems>("items");

	MessageItems items;
	items.append(P4Pair("cmpfile", ""));
	items.append(P4Pair("client", "80"));
	items.append(P4Pair("tag", ""));
	items.append(P4Pair("api", "99999"));
	items.append(P4Pair("enableStreams", ""));
	items.append(P4Pair("expandAndmaps", ""));
	items.append(P4Pair("host", "AMDC.local"));
	items.append(P4Pair("port", "106.120.130.174@1888"));
	items.append(P4Pair("sndbuf", "523552"));
	items.append(P4Pair("rcvbuf", "525600"));
	items.append(P4Pair("func", "protocol"));

	QTest::newRow("items_row") << items;
}

void P4MessageTest::copy()
{
	QFETCH(MessageItems, items);

	P4Message msg;
	foreach (P4Pair p, items)
	{
		msg.appendItem(p.first, p.second);
	}

	QByteArray ba(msg.constructData());
	P4Message msg2(ba);

	QCOMPARE(items, msg2.itemList());
}

void P4MessageTest::itemAddRemove()
{
	P4Message msg;
	QCOMPARE(msg.itemsCount(), 0);

	msg.appendItem("test", "value");
	QCOMPARE(msg.itemsCount(), 1);

	msg.appendItem("test2", "value2");
	QCOMPARE(msg.itemsCount(), 2);

	msg.removeItem("test");
	QCOMPARE(msg.itemsCount(), 1);
	QCOMPARE(msg.containsKey("test"), false);
	QCOMPARE(msg.containsKey("test2"), true);
	QCOMPARE(msg.value("test2"), "value2");

	msg.clearItems();
	QCOMPARE(msg.itemsCount(), 0);
}
