#ifndef P4MESSAGETEST_H
#define P4MESSAGETEST_H

#include <QtCore/QObject>

class P4MessageTest : public QObject
{
	Q_OBJECT
private slots:
	void copy_data();
	void copy();
	void itemAddRemove();
};

#endif // P4MESSAGETEST_H
