/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "P4RequestsTest.h"
#include "request/P4RequestUsers.h"
#include "P4Server.h"
#include "P4Client.h"
#include <QtCore/QCoreApplication>

#define USER "user_0"
#define HOST "localhost"
#define PORT 1666
#define DB_DIR "db"

#define NORMAL_USER "user_0"
#define ADMIN_USER "user_admin"

void P4RequestsTest::noServerConnection()
{
//	P4Client client;
//	client.setHost(HOST, PORT);

	// TODO Fix this test
//	QCOMPARE(res.toString().isEmpty(), false);
}

void P4RequestsTest::clientRequestUsers()
{
	/* FIME Fix this test
	P4Server server;
	server.start(DB_DIR);

	P4RequestUsers *reqUsers = new P4RequestUsers();
	P4Client p4Client(USER, HOST, PORT);
	int argc = 0;
	QCoreApplication app(argc, nullptr);

	QObject::connect(reqUsers, &P4RequestUsers::usersChanged,
					 this, &P4RequestsTest::on_usersChanged);

	QObject::connect(reqUsers, &P4RequestUsers::finished,
					 this, &P4RequestsTest::on_usersFinished);

	QObject::connect(reqUsers, &P4RequestUsers::finished,
					 &app, &QCoreApplication::quit);

	m_users.clear();
	m_res.reset();

	p4Client.send(reqUsers);
	app.exec();

	QCOMPARE(m_users.count(), 2);
	QCOMPARE(m_users.at(0).name(), QString(NORMAL_USER));
	QCOMPARE(m_users.at(1).name(), QString(ADMIN_USER));
	*/
}

void P4RequestsTest::on_usersChanged(const QList<P4User> users)
{
	m_users = users;
}

void P4RequestsTest::on_usersFinished(const QString& cmd, const P4Result& res, int msDuration)
{
	Q_UNUSED(cmd)
	Q_UNUSED(msDuration)
	m_res = res;
}
