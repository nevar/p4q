/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef P4REQUESTSTEST_H
#define P4REQUESTSTEST_H

#include "data/P4User.h"
#include "data/P4Result.h"
#include <QtTest/QtTest>

class P4RequestsTest : public QObject
{
	Q_OBJECT
private slots:
	void noServerConnection();
	void clientRequestUsers();

public slots:
	void on_usersChanged(const QList<P4User> users);
	void on_usersFinished(const QString& cmd, const P4Result& res, int msDuration);

private:
	P4Result m_res;
	QList<P4User> m_users;
};

#endif // P4REQUESTSTEST_H
