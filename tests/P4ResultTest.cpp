/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "p4q/data/P4Result.h"
#include "p4q/data/P4Result_p.h"
#include "P4ResultTest.h"
#include "perforce/clientapi.h"

Q_DECLARE_METATYPE(P4ResultType)
Q_DECLARE_METATYPE(QList<Error>)

void P4ResultTest::all_data()
{
	QTest::addColumn<P4ResultType>("type");
	QTest::addColumn<QString>("prefix");
	QTest::addColumn<QString>("msg");
	QTest::addColumn<bool>("isErr");
	QTest::addColumn<bool>("isEmp");

	QTest::newRow("info") << P4RESULT_INFO       << "[INF] " << "info message" << false << false;
	QTest::newRow("warning") << P4RESULT_WARNING << "[WRN] " <<  "warning message" << false << false;
	QTest::newRow("failed") << P4RESULT_ERROR    << "[ERR] " << "failed message" << true << false;
	QTest::newRow("fatal") << P4RESULT_ERROR     << "[ERR] " <<   "fatal message" << true << false;
	QTest::newRow("empty") << P4RESULT_INFO      << "" <<   "" << false << true;
}

void P4ResultTest::set_data()
{
	all_data();
}

void P4ResultTest::set()
{
	QFETCH(P4ResultType, type);
	QFETCH(QString, prefix);
	QFETCH(QString, msg);
	QFETCH(bool, isErr);
	QFETCH(bool, isEmp);

	P4Result res(msg, type);

	QCOMPARE(res.isError(), isErr);
	QCOMPARE(res.isEmpty(), isEmp);
	QCOMPARE(res.toString(), prefix + msg);
}
