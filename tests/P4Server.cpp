/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "P4Server.h"
#include "QtCore/QDebug"
#include "QtTest/QTest"

P4Server::P4Server()
	: QObject(nullptr)
{
}

void P4Server::start(const QString dbName)
{
	QObject::connect(&m_process, &QProcess::readyRead,
					 this, &P4Server::readyRead);

	m_process.setReadChannel(QProcess::StandardOutput);
	m_process.setProcessChannelMode(QProcess::MergedChannels);
	m_process.setWorkingDirectory(QString(P4D_DIR) + dbName);
	qDebug() << "Set working directory: " << m_process.workingDirectory();

	QStringList args;
	args << "-p" << "localhost:1666" << "-r" << (QString(P4D_DIR) + dbName) << "-f" << "-v 0";
	m_process.start(QString("p4d"), args);
	qDebug() << "start" << m_process.program() << m_process.arguments().join(' ');

	m_process.waitForStarted();
	if (QProcess::Running != m_process.state())
	{
		qDebug() << m_process.readAll();
		qDebug() << "Server failed to start" << m_process.state() << m_process.errorString();
		exit(0);
	}

	// Server is fully operating after some time from start
	QTest::qSleep(500);
}

void P4Server::stop()
{
	m_process.terminate();
}

void P4Server::readyRead()
{
	qDebug() << m_process.readAll();
}
