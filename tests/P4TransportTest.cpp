#include "P4TransportTest.h"
#include "P4Server.h"
#include <p4q/P4Transport.h>

#define USER "user"
#define HOST "localhost"
#define PORT 1666

#define DB_DIR "db"

void P4TransportTest::notConnected()
{
	P4Transport transport;
	P4Result res = transport.connectToHostSync(HOST, PORT);
	QCOMPARE(res.isError(), true);
}

void P4TransportTest::connected()
{
	P4Server srv;
	srv.start(DB_DIR);

	P4Transport transport;
	P4Result res = transport.connectToHostSync(HOST, PORT);
	QCOMPARE(res.isError(), false);

	// pre
	QCOMPARE(transport.host(), HOST);
	QCOMPARE(transport.port(), PORT);
	QCOMPARE(transport.getAllMessages().size(), 0);

	srv.stop();
}

void P4TransportTest::sendMessage()
{
	P4Server srv;
	srv.start(DB_DIR);

	P4Transport transport;
	P4Result res = transport.connectToHostSync(HOST, PORT);
	QCOMPARE(res.isError(), false);

	P4Message msg;

	// send protocol init message
	msg.appendItem("cmpfile", "");
	msg.appendItem("client", "80");
	msg.appendItem("tag", "");
	msg.appendItem("api", "99999");
	msg.appendItem("enableStreams", "");
	msg.appendItem("expandAndmaps", "");
	msg.appendItem("host", "AMDC.local");
	msg.appendItem("port", "106.120.130.174@1888");
	msg.appendItem("sndbuf", "523552");
	msg.appendItem("rcvbuf", "525600");
	msg.appendItem("func", "protocol");
	transport.send(msg);

	// we dont need to wait because "protocol" has no response
	// wait function should just return with 0 messages
	QCOMPARE(transport.waitForMessages(), true);
	QCOMPARE(transport.getAllMessages().count(), 0);

	// send get users message
	msg.clearItems();
	msg.appendItem("client", "");
	msg.appendItem("cwd", "");
	msg.appendItem("os", "UNIX");
	msg.appendItem("user", "root");
	msg.appendItem("charset", "1");
	msg.appendItem("func", "user-users");
	transport.send(msg);

	QCOMPARE(transport.waitForMessages(), true);
	QCOMPARE(transport.getAllMessages().count() > 0, true);

	srv.stop();
}
