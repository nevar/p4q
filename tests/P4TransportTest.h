#ifndef P4TRANSPORTTEST_H
#define P4TRANSPORTTEST_H

#include <QtTest/QtTest>

class P4TransportTest : public QObject
{
	Q_OBJECT
private slots:
	void notConnected();
	void connected();
	void sendMessage();
};

#endif // P4TRANSPORTTEST_H