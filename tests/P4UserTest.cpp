/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "p4q/data/P4User.h"
#include "P4UserTest.h"

typedef QHash<QString,QString> QH;

Q_DECLARE_METATYPE(QH)

void P4UserTest::user_data()
{
	QTest::addColumn<QH>("data");
	QTest::addColumn<QString>("name");

	QH validUserData;
	validUserData.insert("noUser", "not_user");
	validUserData.insert("User", "sample_user");
	validUserData.insert("Userno", "not_user");
	QTest::newRow("valid user") << validUserData << QString("sample_user");

	QH invalidUserData;
	invalidUserData.insert("noUser", "not_user");
	invalidUserData.insert("notUser", "not sample_user");
	invalidUserData.insert("Userno", "not_user");

	QTest::newRow("invalid user") << invalidUserData << QString("");
}

void P4UserTest::user()
{
	QFETCH(QH, data);
	QFETCH(QString, name);

	P4User u(data);

	QCOMPARE(u.name(), name);
}

void P4UserTest::copy()
{
	QH data0;
	data0.insert("User", "sample_user0");

	QH data1;
	data1.insert("User", "sample_user1");

	P4User u0(data0);
	P4User u1(data1);
	P4User u2(u0);

	QCOMPARE(u0.name(), QString("sample_user0"));
	QCOMPARE(u1.name(), QString("sample_user1"));
	QCOMPARE(u2.name(), QString("sample_user0"));

	u0 = u1;

	QCOMPARE(u0.name(), QString("sample_user1"));
	QCOMPARE(u1.name(), QString("sample_user1"));
	QCOMPARE(u2.name(), QString("sample_user0"));
}
