/*
 * Copyright (c) 2017 Rafal Jastrzebski <dev@nevar.pl>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "P4MessageTest.h"
#include "P4ClientTest.h"
#include "P4ResultTest.h"
#include "P4UserTest.h"
#include "P4TransportTest.h"
#include "P4ConnectionTest.h"

int main(int argc, char *argv[])
{
	QTest::qExec(new P4ResultTest, argc, argv);
	QTest::qExec(new P4MessageTest, argc, argv);
	QTest::qExec(new P4TransportTest, argc, argv);
	QTest::qExec(new P4ConnectionTest, argc, argv);

	QTest::qExec(new P4UserTest, argc, argv);
	QTest::qExec(new P4ClientTest, argc, argv);

	return 0;
}
